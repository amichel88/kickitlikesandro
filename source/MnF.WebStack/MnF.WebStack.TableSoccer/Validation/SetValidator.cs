﻿//-----------------------------------------------------------------------
// <copyright file="SetValidator.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System.Data.Entity;
    using System.Linq;

    using FluentValidation;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.WebApi.Validation;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Validator to validate incoming Player-objects
    /// </summary>
    public class SetValidator : AbstractTsValidator<Set, DomainModel.Set>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetValidator" /> class.
        /// </summary>
        /// <param name="setRepository">The set repository.</param>
        /// <param name="matchRepository">The match repository.</param>
        public SetValidator(IRepository<DomainModel.Set> setRepository, IRepository<DomainModel.Match> matchRepository)
            : base(setRepository)
        {
            this.RuleFor(s => s)
                .Must(s => matchRepository.Entities.Any(entry => entry.Id == s.MatchId))
                .WithMessage(set => $"There is no match with ID = {set.MatchId} : Cannot add set");

            this.RuleFor(s => s.GetHashCode())
                .Must((s, _) => !setRepository.Entities.Any(entry => entry.MatchId == s.MatchId && entry.Number == s.Number))
                .When(s => this.IsPost)
                .WithMessage(s => $"A set with this primary key [MatchId, Number] == [{s.MatchId}, {s.Number}] already exists");

            this.RuleFor(s => s.MatchId).MatchMustNotBeCompleted(matchRepository);
        }

        /// <summary>
        /// See "returns" part
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="entities">The entities.</param>
        /// <returns>
        /// true if the entry exists in the database
        /// </returns>
        protected override bool DoesEntryExist(Set resource, IDbSet<DomainModel.Set> entities)
        {
            return entities.Any(domain => resource.MatchId == domain.MatchId && resource.Number == domain.Number);
        }
    }
}
