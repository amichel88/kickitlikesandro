﻿//-----------------------------------------------------------------------
// <copyright file="MatchValidator.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System.Data.Entity;
    using System.Linq;

    using FluentValidation;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.WebApi.Validation;
    using MnF.WebStack.TableSoccer.DomainModel;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Validator to validate incoming Player-objects
    /// </summary>
    public class MatchValidator : AbstractTsValidator<MatchWrite, Match>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatchValidator" /> class.
        /// </summary>
        /// <param name="playerRepository">The player repository.</param>
        /// <param name="matchRepository">The match repository.</param>
        public MatchValidator(IRepository<Match> matchRepository, IRepository<DomainModel.Player> playerRepository)
            : base(matchRepository)
        {
            this.RuleFor(m => m.RedPlayerOneId).NotNull();
            this.RuleFor(m => m.BluePlayerOneId).NotNull();

            this.RuleFor(m => m).PlayerMustNotBeDuplicate();
            this.RuleFor(m => m.Id as int?).MatchMustNotBeCompleted(matchRepository);

            this.RuleFor(m => m.RedPlayerOneId).PlayerMustExist(playerRepository);
            this.RuleFor(m => m.RedPlayerTwoId).PlayerMustExist(playerRepository);
            this.RuleFor(m => m.BluePlayerOneId).PlayerMustExist(playerRepository);
            this.RuleFor(m => m.BluePlayerTwoId).PlayerMustExist(playerRepository);
        }

        /// <summary>
        /// See "returns" part
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="entities">The entities.</param>
        /// <returns>
        /// true if the entry exists in the database
        /// </returns>
        protected override bool DoesEntryExist(MatchWrite resource, IDbSet<Match> entities)
        {
            return entities.Any(domain => resource.Id == domain.Id);
        }
    }
}
