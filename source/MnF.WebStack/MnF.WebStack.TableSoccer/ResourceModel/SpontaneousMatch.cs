﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatch.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.ResourceModel
{
    using System;

    /// <summary>
    /// Represents a table soccer match.
    /// </summary>
    public class SpontaneousMatch
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the time stamp in UTC.
        /// </summary>
        public DateTime TimeStampCreated { get; set; }

        /// <summary>
        /// Gets or sets the time stamp completed.
        /// </summary>
        public DateTime? TimeStampCompleted { get; set; }

        /// <summary>
        /// Gets or sets the score blue.
        /// </summary>
        public int ScoreBlue { get; set; }

        /// <summary>
        /// Gets or sets the score red.
        /// </summary>
        public int ScoreRed { get; set; }
    }
}
