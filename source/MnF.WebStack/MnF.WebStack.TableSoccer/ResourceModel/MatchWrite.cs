﻿//-----------------------------------------------------------------------
// <copyright file="MatchWrite.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.ResourceModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Resource representing a match.
    /// </summary>
    public sealed class MatchWrite
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        [Required]
        public int? BluePlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        public int? BluePlayerTwoId { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        [Required]
        public int? RedPlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        public int? RedPlayerTwoId { get; set; }
    }
}
