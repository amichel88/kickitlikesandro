﻿//-----------------------------------------------------------------------
// <copyright file="Set.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.ResourceModel
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Represents a set of a <see cref="Set"/>.
    /// </summary>
    public class Set
    {
        /// <summary>
        /// Gets or sets the match identifier.
        /// </summary>
        [Required]
        public int? MatchId { get; set; }

        /// <summary>
        /// Gets or sets the number of this set in the match.
        /// </summary>
        [Required]
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets the score of the red team.
        /// </summary>
        public int RedScore { get; set; }

        /// <summary>
        /// Gets or sets the score of the blue team.
        /// </summary>
        public int BlueScore { get; set; }
    }
}
