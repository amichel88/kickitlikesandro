﻿//-----------------------------------------------------------------------
// <copyright file="ISpontaneousMatchService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business
{
    using System.Threading.Tasks;

    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="SpontaneousMatch" /> resource.
    /// </summary>
    public interface ISpontaneousMatchService
    {
        /// <summary>
        /// Gets the current match.
        /// </summary>
        /// <returns>The current match</returns>
        Task<SpontaneousMatch> GetCurrentMatch();

        /// <summary>
        /// Creates the match.
        /// </summary>
        /// <returns>The created match</returns>
        Task<SpontaneousMatch> CreateMatch();

        /// <summary>
        /// Ends and creates the match.
        /// </summary>
        /// <returns>The created match</returns>
        Task<SpontaneousMatch> ResetMatch();

        /// <summary>
        /// Ends and creates the match.
        /// </summary>
        /// <returns>The created match</returns>
        Task<SpontaneousMatch> UndoMatch();

        /// <summary>
        /// Increments the blue.
        /// </summary>
        /// <returns>The changed match</returns>
        Task<SpontaneousMatch> IncrementBlue();

        /// <summary>
        /// Decrements the blue.
        /// </summary>
        /// <returns>The changed match</returns>
        Task<SpontaneousMatch> DecrementBlue();

        /// <summary>
        /// Increments the red.
        /// </summary>
        /// <returns>The changed match</returns>
        Task<SpontaneousMatch> IncrementRed();

        /// <summary>
        /// Decrements the red.
        /// </summary>
        /// <returns>The changed match</returns>
        Task<SpontaneousMatch> DecrementRed();

        /// <summary>
        /// Ends the match.
        /// </summary>
        /// <returns>The completed match</returns>
        Task<SpontaneousMatch> EndMatch();

        /// <summary>
        /// Triggers the server push.
        /// </summary>
        void TriggerServerPush();
    }
}
