﻿//-----------------------------------------------------------------------
// <copyright file="MatchService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="ResourceModel.MatchWrite"/> and <see cref="ResourceModel.MatchRead"/>resources.
    /// </summary>
    internal sealed class MatchService : IMatchService
    {
        private readonly IRepository<DomainModel.Match> matchRepository;
        private readonly IRepository<DomainModel.MatchChange> matchChangeRepository;
        private readonly IConfigurationProvider mapperConfig;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchService" /> class.
        /// </summary>
        /// <param name="matchRepository">The match repository.</param>
        /// <param name="matchChangeRepository">The match change repository.</param>
        /// <param name="mapperConfig">The mapper configuration.</param>
        public MatchService(IRepository<DomainModel.Match> matchRepository, IRepository<DomainModel.MatchChange> matchChangeRepository, IConfigurationProvider mapperConfig)
        {
            this.matchRepository = matchRepository;
            this.matchChangeRepository = matchChangeRepository;
            this.mapperConfig = mapperConfig;
        }

        /// <summary>
        /// Gets all matchs.
        /// </summary>
        /// <returns>
        /// All matchs.
        /// </returns>
        public async Task<IEnumerable<MatchRead>> GetAll()
        {
            return await this.matchRepository.Entities
                       .Where(m => !m.IsDeleted)
                       .OrderByDescending(match => match.TimeStamp)
                       .ProjectTo<MatchRead>(this.mapperConfig)
                       .ToListAsync();
        }

        /// <summary>
        /// Gets the single match with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The match; or <c>null</c> if the specified identifier is unknown.
        /// </returns>
        public async Task<MatchRead> GetById(int id)
        {
            return await this.matchRepository.Entities
                       .Where(m => !m.IsDeleted)
                       .Where(m => m.Id == id)
                       .ProjectTo<MatchRead>(this.mapperConfig)
                       .SingleOrDefaultAsync();
        }

        /// <summary>
        /// Adds the specified match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <remarks>
        /// After successfully adding the match, its identifier is set.
        /// </remarks>
        public async Task Add(MatchWrite match)
        {
            var domain = this.mapperConfig.CreateMapper().Map<MatchWrite, DomainModel.Match>(match);

            this.matchRepository.Entities.Add(domain);
            await this.matchRepository.SaveChangesAsync();
            match.Id = domain.Id;
        }

        /// <summary>
        /// Updates the specified match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <exception cref="KeyNotFoundException">Thrown when the specified match is unknown.</exception>
        public async Task Update(MatchWrite match)
        {
            var domain = await this.matchRepository.Entities.SingleOrDefaultAsync(m => m.Id == match.Id);

            if (domain is null)
            {
                throw new KeyNotFoundException($"No match found with id {match.Id}");
            }

            await this.SaveChangeEntry(domain);

            this.mapperConfig.CreateMapper().Map(match, domain);
            await this.matchRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        public async Task Delete(int id)
        {
            var domain = await this.matchRepository.Entities.SingleOrDefaultAsync(s => s.Id == id);
            if (domain is null)
            {
                return;
            }

            if (domain.IsCompleted)
            {
                throw new ActionNotSupportedException("This match is already completed! Therefore, it cannot be deleted");
            }

            await this.SaveChangeEntry(domain);

            domain.IsDeleted = true;
            await this.matchRepository.SaveChangesAsync();
        }

        private async Task SaveChangeEntry(DomainModel.Match match)
        {
            var changeEntry = this.mapperConfig.CreateMapper().Map<DomainModel.Match, DomainModel.MatchChange>(match);
            changeEntry.ChangeTimeStamp = DateTime.Now;

            this.matchChangeRepository.Entities.Add(changeEntry);
            await this.matchChangeRepository.SaveChangesAsync();
        }
    }
}
