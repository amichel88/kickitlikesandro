﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatchService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="ResourceModel.SpontaneousMatch"/> resource.
    /// </summary>
    public class SpontaneousMatchService : ISpontaneousMatchService
    {
        private readonly IRepository<DomainModel.SpontaneousMatch> spontanousMatchRepository;
        private readonly IRepository<DomainModel.SpontaneousMatchHistory> spontanousMatchHistoryRepository;
        private readonly IConfigurationProvider mapperConfig;
        private readonly Notifier notifier;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpontaneousMatchService" /> class.
        /// </summary>
        /// <param name="spontanousMatchRepository">The spontanous match repository.</param>
        /// <param name="spontanousMatchHistoryRepository">history</param>
        /// <param name="mapperConfig">The mapper configuration.</param>
        /// <param name="notifier">The notifier.</param>
        public SpontaneousMatchService(
            IRepository<DomainModel.SpontaneousMatch> spontanousMatchRepository,
            IRepository<DomainModel.SpontaneousMatchHistory> spontanousMatchHistoryRepository,
            IConfigurationProvider mapperConfig,
            Notifier notifier)
        {
            this.spontanousMatchRepository = spontanousMatchRepository;
            this.spontanousMatchHistoryRepository = spontanousMatchHistoryRepository;
            this.mapperConfig = mapperConfig;
            this.notifier = notifier;
        }

        /// <summary>
        /// Gets all sets.
        /// </summary>
        /// <returns>
        /// All sets.
        /// </returns>
        public async Task<SpontaneousMatch> GetCurrentMatch()
        {
            var currentMatch = await this.GetCurrentMatchAsDm();
            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Creates the match.
        /// </summary>
        /// <returns>
        /// The created match
        /// </returns>
        public async Task<SpontaneousMatch> CreateMatch()
        {
            // save in history
            await this.SaveHistoryAsync(0, 0);

            // update score
            DomainModel.SpontaneousMatch newMatch = new DomainModel.SpontaneousMatch() { TimeStampCreated = DateTime.Now };
            this.spontanousMatchRepository.Entities.Add(newMatch);
            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(newMatch);
        }

        /// <summary>
        /// Ende and creates the match.
        /// </summary>
        /// <returns>
        /// The created match
        /// </returns>
        public async Task<SpontaneousMatch> ResetMatch()
        {
            // save in history
            await this.SaveHistoryAsync(0, 0);

            // reset score
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();
            currentMatch.TimeStampCompleted = DateTime.Now;
            DomainModel.SpontaneousMatch newMatch = new DomainModel.SpontaneousMatch() { TimeStampCreated = DateTime.Now };
            this.spontanousMatchRepository.Entities.Add(newMatch);

            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Ende and creates the match.
        /// </summary>
        /// <returns>
        /// The created match
        /// </returns>
        public async Task<SpontaneousMatch> UndoMatch()
        {
            // get last history entry
            DomainModel.SpontaneousMatchHistory latestEntry = null;
            try
            {
                latestEntry = await this.spontanousMatchHistoryRepository.Entities
                       .OrderByDescending(history => history.TimeStamp)
                       .FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // get current match
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();

            // check latest entry
            if (latestEntry == null)
            {
                return this.GetAsResourceModel(currentMatch);
            }

            // undo current match
            currentMatch.ScoreBlue -= latestEntry.ScoreBlueDiff;
            currentMatch.ScoreRed -= latestEntry.ScoreRedDiff;

            // save in history
            await this.SaveHistoryAsync(-latestEntry.ScoreBlueDiff, -latestEntry.ScoreRedDiff);

            await this.spontanousMatchRepository.SaveChangesAsync();
            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Increments the blue.
        /// </summary>
        /// <returns>
        /// The changed match
        /// </returns>
        public async Task<SpontaneousMatch> IncrementBlue()
        {
            // save in history
            await this.SaveHistoryAsync(1, 0);

            await this.spontanousMatchRepository.SaveChangesAsync();

            // update match score
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();
            currentMatch.ScoreBlue++;
            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Decrements the blue.
        /// </summary>
        /// <returns>
        /// The changed match
        /// </returns>
        public async Task<SpontaneousMatch> DecrementBlue()
        {
            // save in history
            await this.SaveHistoryAsync(-1, 0);

            // update score
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();
            currentMatch.ScoreBlue--;
            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Increments the red.
        /// </summary>
        /// <returns>
        /// The changed match
        /// </returns>
        public async Task<SpontaneousMatch> IncrementRed()
        {
            // save in history
            await this.SaveHistoryAsync(0, 1);

            // update score
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();
            currentMatch.ScoreRed++;
            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Decrements the red.
        /// </summary>
        /// <returns>
        /// The changed match
        /// </returns>
        public async Task<SpontaneousMatch> DecrementRed()
        {
            // save in history
            await this.SaveHistoryAsync(0, -1);

            // update score
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();
            currentMatch.ScoreRed--;
            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Ends the match.
        /// </summary>
        /// <returns>
        /// The completed match
        /// </returns>
        public async Task<SpontaneousMatch> EndMatch()
        {
            // save in history
            await this.SaveHistoryAsync(0, 0);

            // update score
            DomainModel.SpontaneousMatch currentMatch = await this.GetCurrentMatchAsDm();
            currentMatch.TimeStampCompleted = DateTime.Now;
            await this.spontanousMatchRepository.SaveChangesAsync();

            return this.GetAsResourceModel(currentMatch);
        }

        /// <summary>
        /// Triggers the server push.
        /// </summary>
        public void TriggerServerPush()
        {
            this.notifier.NotifyChange();
        }

        private async Task<DomainModel.SpontaneousMatch> GetCurrentMatchAsDm()
        {
            return await this.spontanousMatchRepository.Entities
                       .Where(match => match.TimeStampCompleted == null)
                       .OrderByDescending(match => match.Id)
                       .FirstOrDefaultAsync();
        }

        private SpontaneousMatch GetAsResourceModel(DomainModel.SpontaneousMatch match)
        {
            return this.mapperConfig.CreateMapper().Map<DomainModel.SpontaneousMatch, SpontaneousMatch>(match);
        }

        private async Task SaveHistoryAsync(int blueDiff, int redDiff)
        {
            try
            {
                DomainModel.SpontaneousMatchHistory newEntry = new DomainModel.SpontaneousMatchHistory()
                {
                    TimeStamp = DateTime.Now,
                    ScoreBlueDiff = blueDiff,
                    ScoreRedDiff = redDiff
                };
                this.spontanousMatchHistoryRepository.Entities.Add(newEntry);
                await this.spontanousMatchHistoryRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
