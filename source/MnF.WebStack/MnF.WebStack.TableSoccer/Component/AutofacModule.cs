﻿//-----------------------------------------------------------------------
// <copyright file="AutofacModule.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Component
{
    using System;
    using System.Reflection;
    using System.Security.Policy;

    using Autofac;
    using FluentValidation;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Infrastructure;

    using Module = Autofac.Module;

    /// <summary>
    /// The <see cref="Autofac"/> module if the <see cref="TableSoccer"/> component.
    /// </summary>
    public sealed class AutofacModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            RegisterBusiness(builder);
            RegisterFluentValidation(builder);
        }

        private static void RegisterBusiness(ContainerBuilder builder)
        {
            builder.RegisterType<Business.Detail.PlayerService>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<Business.Detail.MatchService>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<Business.Detail.SetService>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<Business.Detail.SpontaneousMatchService>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterType<Business.Detail.Notifier>().AsImplementedInterfaces().AsSelf().SingleInstance();
        }

        private static void RegisterFluentValidation(ContainerBuilder builder)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            builder.RegisterAssemblyTypes(assemblies)
               .Where(t => t.IsClosedTypeOf(typeof(IValidator<>)))
               .AsImplementedInterfaces()
               .InstancePerRequest();
        }
    }
}
