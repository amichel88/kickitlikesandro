﻿//-----------------------------------------------------------------------
// <copyright file="MappingUtil.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Component
{
    using System;
    using System.Linq.Expressions;
    using AutoMapper;

    /// <summary>
    /// Utility class for mapping
    /// </summary>
    public static class MappingUtil
    {
        /// <summary>
        /// Ignores the specified selector for mapping.
        ///
        /// Beware: The order of the Generics in the <see cref="Profile.CreateMap(Type, Type)"/> is relevant!
        ///
        /// Usecase: In the mapping A=>B, there is a member X on B for which there is no corresponding member on A.
        /// Therefore, you want to ignore B.X. Consequently, B needs to be the second generic type in <see cref="Profile.CreateMap(Type, Type)"/>!
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="map">The map to change.</param>
        /// <param name="sourceSelector">The selector: a lambda-expression (item) => item.MemberToIgnore selceting the member to be ignored.</param>
        /// <returns>The altered map</returns>
        public static IMappingExpression<TSource, TDestination> IgnoreMember<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> sourceSelector)
        {
            map.ForMember(sourceSelector, dest => dest.Ignore());
            return map;
        }
    }
}
