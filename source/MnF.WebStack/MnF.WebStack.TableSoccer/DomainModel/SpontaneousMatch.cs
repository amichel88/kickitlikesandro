﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatch.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System;
    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Represents a table soccer match.
    /// </summary>
    public class SpontaneousMatch
    {
        private DateTime timeStampCreated;
        private DateTime? timeStampCompleted;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the time stamp in UTC.
        /// </summary>
        public DateTime TimeStampCreated
        {
            get => this.timeStampCreated;
            set => this.timeStampCreated = value.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the time stamp completed.
        /// </summary>
        public DateTime? TimeStampCompleted
        {
            get => this.timeStampCompleted;
            set => this.timeStampCompleted = value?.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the score blue.
        /// </summary>
        public int ScoreBlue { get; set; }

        /// <summary>
        /// Gets or sets the score red.
        /// </summary>
        public int ScoreRed { get; set; }
    }
}
