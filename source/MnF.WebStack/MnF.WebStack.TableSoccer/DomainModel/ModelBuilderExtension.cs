﻿//-----------------------------------------------------------------------
// <copyright file="ModelBuilderExtension.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System.Data.Entity;

    /// <summary>
    /// Extension methods
    /// </summary>
    public static class ModelBuilderExtension
    {
        /// <summary>
        /// Configures the specified model builder.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public static void ConfigureTableSoccer(this DbModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureMatch();
            modelBuilder.ConfigurePlayer();
            modelBuilder.ConfigureSet();
            modelBuilder.ConfigureMatchChange();
            modelBuilder.ConfigureSetChange();
            modelBuilder.ConfigureSpontaneousMatches();
        }

        private static void ConfigureMatch(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Match>()
                .HasRequired(m => m.BluePlayerOne)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Match>()
                .HasOptional(m => m.BluePlayerTwo)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Match>()
                .HasRequired(m => m.RedPlayerOne)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Match>()
                .HasOptional(m => m.RedPlayerTwo)
                .WithMany()
                .WillCascadeOnDelete(false);
        }

        private static void ConfigureMatchChange(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MatchChange>()
                .HasRequired(m => m.Match)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MatchChange>()
                .HasRequired(m => m.BluePlayerOne)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MatchChange>()
                .HasOptional(m => m.BluePlayerTwo)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MatchChange>()
                .HasRequired(m => m.RedPlayerOne)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MatchChange>()
                .HasOptional(m => m.RedPlayerTwo)
                .WithMany()
                .WillCascadeOnDelete(false);
        }

        private static void ConfigurePlayer(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>().Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Player>().Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);
        }

        private static void ConfigureSet(this DbModelBuilder modelBuilder)
        {
            // Set has a combined primary key
            modelBuilder.Entity<Set>().HasKey(s => new { s.MatchId, s.Number });

            // One-to-many relationship between match and set
            modelBuilder.Entity<Set>()
                .HasRequired(s => s.Match)
                .WithMany(m => m.Sets)
                .HasForeignKey(s => s.MatchId)
                .WillCascadeOnDelete();
        }

        private static void ConfigureSetChange(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SetChange>()
                .HasRequired(s => s.Match)
                .WithMany()
                .WillCascadeOnDelete(false);
        }

        private static void ConfigureSpontaneousMatches(this DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SpontaneousMatch>().Property(t => t.TimeStampCompleted).IsOptional();
        }
    }
}
