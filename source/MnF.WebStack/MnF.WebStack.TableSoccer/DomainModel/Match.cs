﻿//-----------------------------------------------------------------------
// <copyright file="Match.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Represents a table soccer match.
    /// </summary>
    public class Match
    {
        private DateTime timeStamp;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the time stamp in UTC.
        /// </summary>
        public DateTime TimeStamp
        {
            get => this.timeStamp;
            set => this.timeStamp = value.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the identifier of the red player one.
        /// </summary>
        public int RedPlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the red player two.
        /// </summary>
        public int? RedPlayerTwoId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the blue player one.
        /// </summary>
        public int BluePlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the blue player two.
        /// </summary>
        public int? BluePlayerTwoId { get; set; }

        /// <summary>
        /// Gets or sets the red player one.
        /// </summary>
        public virtual Player RedPlayerOne { get; set; }

        /// <summary>
        /// Gets or sets the red player two.
        /// </summary>
        public virtual Player RedPlayerTwo { get; set; }

        /// <summary>
        /// Gets or sets the blue player one.
        /// </summary>
        public virtual Player BluePlayerOne { get; set; }

        /// <summary>
        /// Gets or sets the blue player two.
        /// </summary>
        public virtual Player BluePlayerTwo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is completed.
        /// </summary>
        [DefaultValue(false)]
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the sets.
        /// </summary>
        public virtual ICollection<Set> Sets { get; set; } = new List<Set>();
    }
}
