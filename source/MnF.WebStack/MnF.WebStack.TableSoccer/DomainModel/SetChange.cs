﻿//-----------------------------------------------------------------------
// <copyright file="SetChange.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Represents a set of a <see cref="Match"/>.
    /// </summary>
    public class SetChange
    {
        private DateTime changeTimeStamp;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the change time stamp.
        /// </summary>
        public DateTime ChangeTimeStamp
        {
            get => this.changeTimeStamp;
            set => this.changeTimeStamp = value.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the match identifier.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the number of this set in the match.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the score of the red team.
        /// </summary>
        [Column("OldRedScore")]
        public int RedScore { get; set; }

        /// <summary>
        /// Gets or sets the score of the blue team.
        /// </summary>
        [Column("OldBlueScore")]
        public int BlueScore { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the match.
        /// </summary>
        public virtual Match Match { get; set; }
    }
}
