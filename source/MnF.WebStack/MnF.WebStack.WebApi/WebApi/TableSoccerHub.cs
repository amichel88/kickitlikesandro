﻿//-----------------------------------------------------------------------
// <copyright file="TableSoccerHub.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi.WebApi
{
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    /// <summary>
    /// SignalR hub for table-soccer resources.
    /// </summary>
    public sealed class TableSoccerHub : Hub<ITableSoccerHubClient>
    {
        private readonly EventPusher eventPusher;
        /// <summary>
        /// Initializes a new instance of the <see cref="TableSoccerHub"/> class.
        /// </summary>
        /// <param name="eventPusher">The eventPusher.</param>
        // ReSharper diable once UnusedParameter.Locl
        public TableSoccerHub(EventPusher eventPusher)
        {
            this.eventPusher = eventPusher;
        }

        public void SignalModelChanged()
        {
            this.eventPusher.OnModelChanged();
        }
    }
}
