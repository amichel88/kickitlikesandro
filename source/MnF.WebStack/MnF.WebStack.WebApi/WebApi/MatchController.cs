﻿//-----------------------------------------------------------------------
// <copyright file="MatchController.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi.WebApi
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// WebApi controller for <see cref="MatchWrite"/> resources.
    /// </summary>
    [RoutePrefix("api/v1/match")]
    public sealed class MatchController : ApiController
    {
        private readonly IMatchService matchService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchController"/> class.
        /// </summary>
        /// <param name="matchService">The player service.</param>
        public MatchController(IMatchService matchService)
        {
            this.matchService = matchService;
        }

        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>The players</returns>
        [Route("")]
        public async Task<IEnumerable<MatchRead>> GetAll()
        {
            return await this.matchService.GetAll();
        }

        /// <summary>
        /// Gets the single player with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// A content result containing the requested match or not found.
        /// </returns>
        [Route("{id:int}", Name = "GetMatchById")] // der Name ist für den Redirect beim put/post nötig: Gebe dieser Route einen Namen
        public async Task<IHttpActionResult> GetById([FromUri]int id)
        {
            var match = await this.matchService.GetById(id);
            if (match is null)
            {
                return this.NotFound();
            }

            return this.Ok(match);
        }

        /// <summary>
        /// Posts (adds) the specified new player.
        /// </summary>
        /// <param name="match">The player.</param>
        /// <returns>A redirection to the new player.</returns>
        [Route("")]
        public async Task<IHttpActionResult> PostNew(MatchWrite match)
        {
            await this.matchService.Add(match);

            return this.CreatedAtRoute("GetMatchById", new { id = match.Id }, match);
        }

        /// <summary>
        /// Puts (updates) the specified existing match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns>
        /// A content result containing the updated match or not found.
        /// </returns>
        [Route("")]
        public async Task<IHttpActionResult> PutUpdate(MatchWrite match)
        {
            try
            {
                await this.matchService.Update(match);

                return this.Ok(match);
            }
            catch (KeyNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The async task</returns>
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete([FromUri] int id)
        {
            await this.matchService.Delete(id);
            return this.Ok();
        }
    }
}
