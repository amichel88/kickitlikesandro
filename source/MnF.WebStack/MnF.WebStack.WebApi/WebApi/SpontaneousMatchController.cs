﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatchController.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi.WebApi
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// WebApi controller for <see cref="SpontaneousMatch"/> resources.
    /// </summary>
    [RoutePrefix("api/v1/spontaneousMatch")]
    public sealed class SpontaneousMatchController : ApiController
    {
        private readonly ISpontaneousMatchService spontaneousMatchService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpontaneousMatchController"/> class.
        /// </summary>
        /// <param name="spontaneousMatchService">The player service.</param>
        public SpontaneousMatchController(ISpontaneousMatchService spontaneousMatchService)
        {
            this.spontaneousMatchService = spontaneousMatchService;
        }

        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>The players</returns>
        [Route("")]
        public async Task<SpontaneousMatch> GetCurrentMatch()
        {
            return await this.spontaneousMatchService.GetCurrentMatch();
        }

        /// <summary>
        /// reset match.
        /// </summary>
        /// <returns>the current match</returns>
        [Route("reset")]
        public async Task<SpontaneousMatch> PostResetMatch()
        {
            var tmp = await this.spontaneousMatchService.ResetMatch();
            await PostTriggerServerPush();
            return tmp;
        }

        /// <summary>
        /// undo match.
        /// </summary>
        /// <returns>the current match</returns>
        [Route("undo")]
        public async Task<SpontaneousMatch> PostUndoMatch()
        {
            var tmp = await this.spontaneousMatchService.UndoMatch();
            await PostTriggerServerPush();
            return tmp;
        }

        /// <summary>
        /// reset match.
        /// </summary>
        /// <returns>The players</returns>
        [Route("incBlue")]
        public async Task<SpontaneousMatch> PostIncBlue()
        {
            var tmp = await this.spontaneousMatchService.IncrementBlue();
            await PostTriggerServerPush();
            return tmp;
        }

        /// <summary>
        /// reset match.
        /// </summary>
        /// <returns>The players</returns>
        [Route("decBlue")]
        public async Task<SpontaneousMatch> PostDecBlue()
        {
            var tmp = await this.spontaneousMatchService.DecrementBlue();
            await PostTriggerServerPush();
            return tmp;
        }

        /// <summary>
        /// reset match.
        /// </summary>
        /// <returns>The players</returns>
        [Route("incRed")]
        public async Task<SpontaneousMatch> PostIncRed()
        {
            var tmp = await this.spontaneousMatchService.IncrementRed();
            await PostTriggerServerPush();
            return tmp;
        }

        /// <summary>
        /// reset match.
        /// </summary>
        /// <returns>The players</returns>
        [Route("decRed")]
        public async Task<SpontaneousMatch> PostDecRed()
        {
            var tmp = await this.spontaneousMatchService.DecrementRed();
            await PostTriggerServerPush();
            return tmp;
        }

        /// <summary>
        /// Triggers the server push.
        /// </summary>
        /// <returns>OK</returns>
        [Route("triggerServerPush")]
        public async Task<IHttpActionResult> PostTriggerServerPush()
        {
            this.spontaneousMatchService.TriggerServerPush();
            return this.Ok();
        }
    }
}
