﻿//-----------------------------------------------------------------------
// <copyright file="SetController.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi.WebApi
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// WebApi controller for <see cref="Set"/> resources.
    /// </summary>
    [RoutePrefix("api/v1/set")]
    public sealed class SetController : ApiController
    {
        private readonly ISetService setService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SetController"/> class.
        /// </summary>
        /// <param name="setService">The player service.</param>
        public SetController(ISetService setService)
        {
            this.setService = setService;
        }

        /// <summary>
        /// Gets all players.
        /// </summary>
        /// <returns>The players</returns>
        [Route("")]
        public async Task<IEnumerable<Set>> GetAll()
        {
            return await this.setService.GetAll();
        }

        /// <summary>
        /// Gets the single player with the specified identifier.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        /// <returns>
        /// A content result containing the requested set or not found.
        /// </returns>
        [Route("byMatch/{matchId:int}")] // der Name ist für den Redirect beim put/post nötig: Gebe dieser Route einen Namen
        public async Task<IHttpActionResult> GetByMatchId([FromUri] int matchId)
        {
            IEnumerable<Set> sets = (await this.setService.GetByMatchId(matchId)).ToList();
            if (sets.IsNullOrEmpty())
            {
                return this.NotFound();
            }

            return this.Ok(sets);
        }

        /// <summary>
        /// Gets the single player with the specified identifier.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        /// <param name="setNumber">The set number.</param>
        /// <returns>
        /// A content result containing the requested set or not found.
        /// </returns>
        [Route("{matchId:int}/{setNumber:int}", Name = "GetSetByPk")] // der Name ist für den Redirect beim put/post nötig: Gebe dieser Route einen Namen
        public async Task<IHttpActionResult> GetByPk([FromUri] int matchId, [FromUri] int setNumber)
        {
            var set = await this.setService.GetByPk(matchId, setNumber);
            if (set is null)
            {
                return this.NotFound();
            }

            return this.Ok(set);
        }

        /// <summary>
        /// Posts (adds) the specified new player.
        /// </summary>
        /// <param name="set">The player.</param>
        /// <returns>A redirection to the new player.</returns>
        [Route("")]
        public async Task<IHttpActionResult> PostNew(Set set)
        {
            await this.setService.Add(set);

            return this.CreatedAtRoute("GetSetByPk", new { matchId = set.MatchId, setNumber = set.Number }, set);
        }

        /// <summary>
        /// Puts (updates) the specified existing set.
        /// </summary>
        /// <param name="set">The set.</param>
        /// <returns>
        /// A content result containing the updated set or not found.
        /// </returns>
        [Route("")]
        public async Task<IHttpActionResult> PutUpdate(Set set)
        {
            try
            {
                await this.setService.Update(set);
                return this.Ok(set);
            }
            catch (KeyNotFoundException)
            {
                return this.NotFound();
            }
        }

        /// <summary>
        /// Deletes the specified match identifier.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        /// <param name="setNumber">The set number.</param>
        /// <returns>The async task</returns>
        [Route("{matchId:int}/{setNumber:int}")]
        public async Task<IHttpActionResult> Delete([FromUri] int matchId, [FromUri] int setNumber)
        {
            await this.setService.Delete(matchId, setNumber);
            return this.Ok();
        }
    }
}
