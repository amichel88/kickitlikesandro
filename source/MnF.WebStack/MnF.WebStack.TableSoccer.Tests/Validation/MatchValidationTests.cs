﻿//-----------------------------------------------------------------------
// <copyright file="MatchValidationTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Autofac.Extras.Moq;

    using FluentAssertions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.ResourceModel;

    using Moq;

    using NUnit.Framework;

    using Player = MnF.WebStack.TableSoccer.DomainModel.Player;

    /// <summary>
    /// Unit tests for the <see cref="MatchValidator"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class MatchValidationTests
    {
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();

        private AutoMock mock;
        private List<DomainModel.Match> testMatches;
        private List<Player> testPlayers;

        private Mock<DbSet<DomainModel.Match>> mockMatchDbSet;
        private Mock<DbSet<Player>> mockPlayerDbSet;

        private MatchValidator validator;

        private static IEnumerable<Player> TestPlayers
        {
            get
            {
                var id = 1;
                yield return new Player { Id = id++, FirstName = "Alain", LastName = "Berset", };
                yield return new Player { Id = id++, FirstName = "Ueli", LastName = "Maurer", };
                yield return new Player { Id = id++, FirstName = "Doris", LastName = "Leuthard", };
                yield return new Player { Id = id++, FirstName = "Simonetta", LastName = "Sommaruga", };
                yield return new Player { Id = id++, FirstName = "Johann", LastName = "Schneider-Ammann", };
                yield return new Player { Id = id++, FirstName = "Guy", LastName = "Parmelin", };
                yield return new Player { Id = id, FirstName = "Ignazio", LastName = "Cassis", };
            }
        }

        private static IEnumerable<DomainModel.Match> TestMatches
        {
            get
            {
                yield return new DomainModel.Match
                                 {
                                     Id = 1,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 1,
                                     RedPlayerTwoId = 2,
                                     BluePlayerOneId = 3,
                                     BluePlayerTwoId = 4,
                                     RedPlayerOne = new Player { Id = 1 },
                                     RedPlayerTwo = new Player { Id = 2 },
                                     BluePlayerOne = new Player { Id = 3 },
                                     BluePlayerTwo = new Player { Id = 4 }
                                 };

                yield return new DomainModel.Match
                                 {
                                     Id = 2,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 2,
                                     RedPlayerTwoId = 3,
                                     BluePlayerOneId = 4,
                                     BluePlayerTwoId = 5,
                                     RedPlayerOne = new Player { Id = 2 },
                                     RedPlayerTwo = new Player { Id = 3 },
                                     BluePlayerOne = new Player { Id = 4 },
                                     BluePlayerTwo = new Player { Id = 5 }
                                 };

                yield return new DomainModel.Match
                                 {
                                     Id = 3,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 3,
                                     RedPlayerTwoId = 4,
                                     BluePlayerOneId = 5,
                                     BluePlayerTwoId = 6,
                                     RedPlayerOne = new Player { Id = 3 },
                                     RedPlayerTwo = new Player { Id = 4 },
                                     BluePlayerOne = new Player { Id = 5 },
                                     BluePlayerTwo = new Player { Id = 6 },
                                     IsCompleted = true,
                                 };
            }
        }

        private static MatchWrite ExistingMatch => CreateMatch(1);

        private static MatchWrite NewMatch => CreateMatch(42);

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testMatches = TestMatches.ToList();
            this.testPlayers = TestPlayers.ToList();

            // provide a match repository containing the test match
            this.mockMatchDbSet = new Mock<DbSet<DomainModel.Match>>().SetupData(this.testMatches);
            this.mock.Mock<IRepository<DomainModel.Match>>().SetupGet(r => r.Entities).Returns(this.mockMatchDbSet.Object);

            // provide a player repository containing the test players
            this.mockPlayerDbSet = new Mock<DbSet<Player>>().SetupData(this.testPlayers);
            this.mock.Mock<IRepository<Player>>().SetupGet(r => r.Entities).Returns(this.mockPlayerDbSet.Object);

            this.validator = this.mock.Create<MatchValidator>();   // must come after creating the mock-repository because it depends on it
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public void ArePlayersUniqueFindsDuplicates()
        {
            var match = new MatchWrite()
                            {
                                BluePlayerOneId = 1,
                                BluePlayerTwoId = 1,
                                RedPlayerOneId = 2,
                                RedPlayerTwoId = 3
                            };

            match.ArePlayersUnique().Should().BeFalse();
        }

        [Test]
        public void ArePlayersUniqueAccepts2UniquePlayers()
        {
            var match = new MatchWrite()
                            {
                                BluePlayerOneId = 1,
                                RedPlayerOneId = 2
                            };

            match.ArePlayersUnique().Should().BeTrue();
        }

        [Test]
        public void ArePlayersUniqueAccepts4UniquePlayers()
        {
            var match = new MatchWrite()
                            {
                                BluePlayerOneId = 1,
                                BluePlayerTwoId = 2,
                                RedPlayerOneId = 3,
                                RedPlayerTwoId = 4
                            };

            match.ArePlayersUnique().Should().BeTrue();
        }

        [Test]
        public void PostNewSucceeds()
        {
            ValidationTestUtil.ValidatePost(NewMatch, this.validator, false);
        }

        [Test]
        public void PutOnExistingSucceeds()
        {
            ValidationTestUtil.ValidatePut(ExistingMatch, this.validator, false);
        }

        [Test]
        public void PutOnNewFails()
        {
            ValidationTestUtil.ValidatePut(NewMatch, this.validator, true);
        }

        [Test]
        public void PostWithTwoPlayersSucceds()
        {
            var match = NewMatch;
            match.BluePlayerTwoId = null;
            match.RedPlayerTwoId = null;
            ValidationTestUtil.ValidatePost(match, this.validator, false);
        }

        [Test]
        public void PostWithMissingPlayersFails()
        {
            var match = NewMatch;
            match.BluePlayerOneId = null;
            ValidationTestUtil.ValidatePost(match, this.validator, true);
        }

        [Test]
        public void PostWithDuplicatePlayersFails()
        {
            var match = NewMatch;
            match.RedPlayerOneId = 1;
            match.RedPlayerTwoId = 1;
            ValidationTestUtil.ValidatePost(match, this.validator, true);

            match = NewMatch;
            match.RedPlayerOneId = 1;
            match.BluePlayerOneId = 1;
            ValidationTestUtil.ValidatePost(match, this.validator, true);
        }

        [Test]
        public void PostWithInvalidPlayerFails()
        {
            var match = NewMatch;
            match.RedPlayerOneId = 42;
            ValidationTestUtil.ValidatePost(match, this.validator, true);

            match = NewMatch;
            match.RedPlayerTwoId = 42;
            ValidationTestUtil.ValidatePost(match, this.validator, true);

            match = NewMatch;
            match.BluePlayerOneId = 42;
            ValidationTestUtil.ValidatePost(match, this.validator, true);

            match = NewMatch;
            match.BluePlayerTwoId = 42;
            ValidationTestUtil.ValidatePost(match, this.validator, true);
        }

        [Test]
        public void PutOnCompletedFails()
        {
            var match = CreateMatch(3);     // the match in TestMatches with Id = 3 has IsCompleted = true
            ValidationTestUtil.ValidatePut(match, this.validator, true);
        }

        private static MatchWrite CreateMatch(int id)
        {
            return new MatchWrite()
                       {
                           Id = id,
                           TimeStamp = Now,
                           RedPlayerOneId = 1,
                           RedPlayerTwoId = 2,
                           BluePlayerOneId = 3,
                           BluePlayerTwoId = 4
                       };
        }
    }
}
