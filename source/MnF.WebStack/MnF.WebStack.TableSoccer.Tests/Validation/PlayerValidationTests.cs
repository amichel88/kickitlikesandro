﻿//-----------------------------------------------------------------------
// <copyright file="PlayerValidationTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Autofac.Extras.Moq;

    using FluentValidation.TestHelper;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.DomainModel;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="PlayerValidator"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class PlayerValidationTests
    {
        private AutoMock mock;
        private List<Player> testPlayers;
        private Mock<DbSet<Player>> mockDbSet;

        private PlayerValidator validator;

        private static IEnumerable<Player> TestPlayers
        {
            get
            {
                var id = 1;
                yield return new Player { Id = id++, FirstName = "Alain", LastName = "Berset", };
                yield return new Player { Id = id++, FirstName = "Ueli", LastName = "Maurer", };
                yield return new Player { Id = id++, FirstName = "Doris", LastName = "Leuthard", };
                yield return new Player { Id = id++, FirstName = "Simonetta", LastName = "Sommaruga", };
                yield return new Player { Id = id++, FirstName = "Johann", LastName = "Schneider-Ammann", };
                yield return new Player { Id = id++, FirstName = "Guy", LastName = "Parmelin", };
                yield return new Player { Id = id, FirstName = "Ignazio", LastName = "Cassis", };
            }
        }

        private static ResourceModel.Player ExistingPlayer => CreatePlayer(1);

        private static ResourceModel.Player NewPlayer => CreatePlayer(42);

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testPlayers = TestPlayers.ToList();

            // provide a player repository containing the test players
            this.mockDbSet = new Mock<DbSet<Player>>().SetupData(this.testPlayers);
            this.mock.Mock<IRepository<Player>>().SetupGet(r => r.Entities).Returns(this.mockDbSet.Object);

            this.validator = this.mock.Create<PlayerValidator>();   // must come after creating the mock-repository because it depends on it
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public void PostNewSucceeds()
        {
            ValidationTestUtil.ValidatePost(NewPlayer, this.validator, false);
        }

        [Test]
        public void PutOnExistingSucceeds()
        {
            ValidationTestUtil.ValidatePut(ExistingPlayer, this.validator, false);
        }

        [Test]
        public void PutOnNewFails()
        {
            ValidationTestUtil.ValidatePut(NewPlayer, this.validator, true);
        }

        [Test]
        public void MissingNamesFail()
        {
            this.validator.ShouldHaveValidationErrorFor(
                p => p.FirstName,
                new ResourceModel.Player() { FirstName = null, LastName = "Test" });

            this.validator.ShouldHaveValidationErrorFor(
                p => p.FirstName,
                new ResourceModel.Player() { FirstName = string.Empty, LastName = "Test" });

            this.validator.ShouldHaveValidationErrorFor(
                p => p.LastName,
                new ResourceModel.Player() { FirstName = "Test", LastName = null });

            this.validator.ShouldHaveValidationErrorFor(
                p => p.LastName,
                new ResourceModel.Player() { FirstName = "Test", LastName = string.Empty });
        }

        [Test]
        public void ValidNamesSucced()
        {
            this.validator.ShouldNotHaveValidationErrorFor(
                p => p.FirstName,
                new ResourceModel.Player() { FirstName = "Test", LastName = "Tester" });

            this.validator.ShouldNotHaveValidationErrorFor(
                p => p.LastName,
                new ResourceModel.Player() { FirstName = "Test", LastName = "Tester" });
        }

        private static ResourceModel.Player CreatePlayer(int id)
        {
            return new ResourceModel.Player() { Id = id, FirstName = "Lucky", LastName = "Luke" };
        }
    }
}
