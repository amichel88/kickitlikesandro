﻿//-----------------------------------------------------------------------
// <copyright file="ValidationTestUtil.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System;
    using System.Linq.Expressions;
    using System.Net.Http;
    using System.Web.Http.Controllers;

    using FluentValidation;
    using FluentValidation.TestHelper;

    using MnF.WebStack.Common.WebApi.Validation;

    /// <summary>
    /// Utility class for validation tests
    /// </summary>
    public static class ValidationTestUtil
    {
        /// <summary>
        /// Validates a post request for the given object using the given validator.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="objToValidate">The object to validate.</param>
        /// <param name="validator">The validator.</param>
        /// <param name="errorExpected">if set to <c>true</c> [error expected].</param>
        public static void ValidatePost<TEntity>(TEntity objToValidate, ITsValidatorGeneric<TEntity> validator, bool errorExpected)
        where TEntity : class
        {
            var httpContext = CreateHttpContextPost();
            validator.SetHttpActionContext(httpContext);

            validator.TestValidation(p => p.GetHashCode(), objToValidate, errorExpected);
        }

        /// <summary>
        /// Validates a put request for the given object using the given validator.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="objToValidate">The object to validate.</param>
        /// <param name="validator">The validator.</param>
        /// <param name="errorExpected">if set to <c>true</c> [error expected].</param>
        public static void ValidatePut<TEntity>(TEntity objToValidate, ITsValidatorGeneric<TEntity> validator, bool errorExpected)
        where TEntity : class
        {
            var httpContext = CreateHttpContextPut();
            validator.SetHttpActionContext(httpContext);

            validator.TestValidation(p => p.GetHashCode(), objToValidate, errorExpected);
        }

        private static HttpActionContext CreateHttpActionContext(HttpMethod method)
        {
            // provide a http action context
            var request = new HttpRequestMessage { Method = method };
            var controllerContext = new HttpControllerContext { Request = request };
            return new HttpActionContext { ControllerContext = controllerContext };
        }

        /// <summary>
        /// Creates the HTTP context for a put request.
        /// </summary>
        /// <returns>The context</returns>
        private static HttpActionContext CreateHttpContextPut()
        {
            return CreateHttpActionContext(HttpMethod.Put);
        }

        /// <summary>
        /// Creates the HTTP context for a post request.
        /// </summary>
        /// <returns>The context</returns>
        private static HttpActionContext CreateHttpContextPost()
        {
            return CreateHttpActionContext(HttpMethod.Post);
        }

        private static void TestValidation<T, TValue>(
            this IValidator<T> validator,
            Expression<Func<T, TValue>> expression,
            T objectToTest,
            bool errorExpected = false,
            string ruleSet = null)
            where T : class
        {
            if (errorExpected)
            {
                validator.ShouldHaveValidationErrorFor(expression, objectToTest, ruleSet);
            }
            else
            {
                validator.ShouldNotHaveValidationErrorFor(expression, objectToTest, ruleSet);
            }
        }
    }
}
