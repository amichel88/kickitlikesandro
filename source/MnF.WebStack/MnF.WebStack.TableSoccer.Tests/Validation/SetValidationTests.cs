﻿//-----------------------------------------------------------------------
// <copyright file="SetValidationTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using Autofac.Extras.Moq;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.DomainModel;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="SetValidator"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class SetValidationTests
    {
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();

        private AutoMock mock;
        private List<DomainModel.Match> testMatches;
        private List<Set> testSets;

        private Mock<DbSet<DomainModel.Match>> mockMatchDbSet;
        private Mock<DbSet<Set>> mockSetDbSet;

        private SetValidator validator;

        private static IEnumerable<Set> TestSets
        {
            get
            {
                yield return new Set
                                 {
                                     MatchId = 1,
                                     Number = 1,
                                     RedScore = 1,
                                     BlueScore = 2
                                 };
                yield return new Set
                                 {
                                     MatchId = 2,
                                     Number = 1,
                                     RedScore = 5,
                                     BlueScore = 2
                                 };
                yield return new Set
                                 {
                                     MatchId = 2,
                                     Number = 2,
                                     RedScore = 1,
                                     BlueScore = 7
                                 };
                yield return new Set
                                 {
                                     MatchId = 3,
                                     Number = 1,
                                     RedScore = 1,
                                     BlueScore = 6
                                 };
            }
        }

        private static IEnumerable<DomainModel.Match> TestMatches
        {
            get
            {
                yield return new DomainModel.Match
                                 {
                                     Id = 1,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 1,
                                     RedPlayerTwoId = 2,
                                     BluePlayerOneId = 3,
                                     BluePlayerTwoId = 4,
                                     RedPlayerOne = new Player { Id = 1 },
                                     RedPlayerTwo = new Player { Id = 2 },
                                     BluePlayerOne = new Player { Id = 3 },
                                     BluePlayerTwo = new Player { Id = 4 }
                                 };

                yield return new DomainModel.Match
                                 {
                                     Id = 2,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 2,
                                     RedPlayerTwoId = 3,
                                     BluePlayerOneId = 4,
                                     BluePlayerTwoId = 5,
                                     RedPlayerOne = new Player { Id = 2 },
                                     RedPlayerTwo = new Player { Id = 3 },
                                     BluePlayerOne = new Player { Id = 4 },
                                     BluePlayerTwo = new Player { Id = 5 }
                                 };

                yield return new DomainModel.Match
                                 {
                                     Id = 3,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 3,
                                     RedPlayerTwoId = 4,
                                     BluePlayerOneId = 5,
                                     BluePlayerTwoId = 6,
                                     RedPlayerOne = new Player { Id = 3 },
                                     RedPlayerTwo = new Player { Id = 4 },
                                     BluePlayerOne = new Player { Id = 5 },
                                     BluePlayerTwo = new Player { Id = 6 },
                                     IsCompleted = true,
                                 };
            }
        }

        private static ResourceModel.Set ExistingSet => CreateSet(1, 1);

        private static ResourceModel.Set NewSetForExistingMatch => CreateSet(1, 42);

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testMatches = TestMatches.ToList();
            this.testSets = TestSets.ToList();

            // provide a match repository containing the test matches
            this.mockMatchDbSet = new Mock<DbSet<DomainModel.Match>>().SetupData(this.testMatches);
            this.mock.Mock<IRepository<DomainModel.Match>>().SetupGet(r => r.Entities).Returns(this.mockMatchDbSet.Object);

            // provide a set repository containing the test sets
            this.mockSetDbSet = new Mock<DbSet<Set>>().SetupData(this.testSets);
            this.mock.Mock<IRepository<Set>>().SetupGet(r => r.Entities).Returns(this.mockSetDbSet.Object);

            this.validator = this.mock.Create<SetValidator>();   // must come after creating the mock-repository because it depends on it
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public void PostNewSucceeds()
        {
            ValidationTestUtil.ValidatePost(NewSetForExistingMatch, this.validator, false);
        }

        [Test]
        public void PutOnExistingSucceeds()
        {
            ValidationTestUtil.ValidatePut(ExistingSet, this.validator, false);
        }

        [Test]
        public void PutOnNewFails()
        {
            ValidationTestUtil.ValidatePut(NewSetForExistingMatch, this.validator, true);
        }

        [Test]
        public void PostSetOnCompletedMatchFails()
        {
            var set = CreateSet(3, 42);
            ValidationTestUtil.ValidatePost(set, this.validator, true);
        }

        [Test]
        public void PostSetOnInvalidMatchFails()
        {
            var set = CreateSet(42, 1);
            ValidationTestUtil.ValidatePost(set, this.validator, true);
        }

        [Test]
        public void PostExistingSetFails()
        {
            var set = CreateSet(1, 1);
            ValidationTestUtil.ValidatePost(set, this.validator, true);
        }

        private static ResourceModel.Set CreateSet(int matchId, int number)
        {
            return new ResourceModel.Set()
                       {
                           MatchId = matchId,
                           Number = number,
                           BlueScore = 3,
                           RedScore = 7
                       };
        }
    }
}
