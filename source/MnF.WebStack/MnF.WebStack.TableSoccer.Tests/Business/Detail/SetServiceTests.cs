﻿//-----------------------------------------------------------------------
// <copyright file="SetServiceTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;

    using Autofac.Extras.Moq;

    using AutoMapper;

    using FluentAssertions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.DomainModel;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="SetService"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class SetServiceTests
    {
        private static readonly IMapper Mapper = new MapperConfiguration(cfg => cfg.AddProfiles(typeof(SetService))).CreateMapper();
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();

        private AutoMock mock;

        private List<Set> testSets;
        private List<SetChange> testSetChanges;

        private Mock<DbSet<Set>> mockSetDbSet;
        private Mock<DbSet<SetChange>> mockSetChangeDbSet;

        private static IEnumerable<DomainModel.Match> TestMatches
        {
            get
            {
                yield return new DomainModel.Match
                                 {
                                     Id = 1,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 1,
                                     RedPlayerTwoId = 2,
                                     BluePlayerOneId = 3,
                                     BluePlayerTwoId = 4,
                                     RedPlayerOne = new Player { Id = 1 },
                                     RedPlayerTwo = new Player { Id = 2 },
                                     BluePlayerOne = new Player { Id = 3 },
                                     BluePlayerTwo = new Player { Id = 4 }
                                 };

                yield return new DomainModel.Match
                                 {
                                     Id = 2,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 2,
                                     RedPlayerTwoId = 3,
                                     BluePlayerOneId = 4,
                                     BluePlayerTwoId = 5,
                                     RedPlayerOne = new Player { Id = 2 },
                                     RedPlayerTwo = new Player { Id = 3 },
                                     BluePlayerOne = new Player { Id = 4 },
                                     BluePlayerTwo = new Player { Id = 5 }
                                 };

                yield return new DomainModel.Match
                                 {
                                     Id = 3,
                                     TimeStamp = Now,
                                     RedPlayerOneId = 3,
                                     RedPlayerTwoId = 4,
                                     BluePlayerOneId = 5,
                                     BluePlayerTwoId = 6,
                                     RedPlayerOne = new Player { Id = 3 },
                                     RedPlayerTwo = new Player { Id = 4 },
                                     BluePlayerOne = new Player { Id = 5 },
                                     BluePlayerTwo = new Player { Id = 6 },
                                     IsCompleted = true,
                                 };
            }
        }

        private static IEnumerable<Set> TestSets
        {
            get
            {
                yield return new Set
                {
                    MatchId = 1,
                    Number = 1,
                    RedScore = 1,
                    BlueScore = 2,
                    Match = TestMatches.SingleOrDefault(m => m.Id == 1)
                };
                yield return new Set
                {
                    MatchId = 2,
                    Number = 1,
                    RedScore = 5,
                    BlueScore = 2,
                    Match = TestMatches.SingleOrDefault(m => m.Id == 2)
                };
                yield return new Set
                {
                    MatchId = 2,
                    Number = 2,
                    RedScore = 1,
                    BlueScore = 7,
                    Match = TestMatches.SingleOrDefault(m => m.Id == 2)
                };
                yield return new Set
                {
                    MatchId = 3,
                    Number = 1,
                    RedScore = 1,
                    BlueScore = 6,
                    Match = TestMatches.SingleOrDefault(m => m.Id == 3)
                };
            }
        }

        private static IEnumerable<SetChange> TestSetChanges
        {
            get
            {
                yield return new SetChange
                {
                    MatchId = 42,
                    Number = 42,
                    RedScore = 1,
                    BlueScore = 2
                };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testSets = TestSets.ToList();
            this.testSetChanges = TestSetChanges.ToList();

            // provide an AutoMapper configuration, loaded with the profiles from the assembly-under-test.
            this.mock.Provide<IConfigurationProvider>(new MapperConfiguration(cfg => cfg.AddProfiles(typeof(SetService))));

            // provide a match repository containing the test sets
            this.mockSetDbSet = new Mock<DbSet<Set>>().SetupData(this.testSets);
            this.mock.Mock<IRepository<Set>>().SetupGet(r => r.Entities).Returns(this.mockSetDbSet.Object);

            // provide a match repository containing the test set changes
            this.mockSetChangeDbSet = new Mock<DbSet<SetChange>>().SetupData(this.testSetChanges);
            this.mock.Mock<IRepository<SetChange>>().SetupGet(r => r.Entities).Returns(this.mockSetChangeDbSet.Object);
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetAllFromEmptyDatabase()
        {
            var localMockDbSet = new Mock<DbSet<Set>>().SetupData(new List<Set>());
            this.mock.Mock<IRepository<Set>>().SetupGet(r => r.Entities).Returns(localMockDbSet.Object);

            var sut = this.mock.Create<SetService>();
            (await sut.GetAll()).Should().BeEmpty();
        }

        [Test]
        public async Task GetAllGetsAll()
        {
            var sut = this.mock.Create<SetService>();
            (await sut.GetAll()).Count().Should().Be(4);
        }

        [Test]
        public async Task GetByPkSuccess()
        {
            var sut = this.mock.Create<SetService>();
            var result = await sut.GetByPk(3, 1);

            var sets = TestSets.ToArray();
            var setToCompare = Mapper.Map<Set, ResourceModel.Set>(sets[3]);

            result.Should().BeEquivalentTo(setToCompare);
        }

        [Test]
        public async Task GetByPkReturnsNullIfPkIsUnknown()
        {
            var sut = this.mock.Create<SetService>();
            (await sut.GetByPk(4711, 1)).Should().BeNull();
            (await sut.GetByPk(3, 4444)).Should().BeNull();
        }

        [Test]
        public async Task GetByMatchIdSuccess()
        {
            var sut = this.mock.Create<SetService>();
            var result = (await sut.GetByMatchId(3)).ToList();

            result.Should().NotBeEmpty();

            this.testSets.Should().Contain(m => result[0].IsEquivalent(m) && m.MatchId == 3);
        }

        [Test]
        public async Task GetByMatchIdReturnsNullIfMatchIdIsUnknown()
        {
            var sut = this.mock.Create<SetService>();
            var result = await sut.GetByMatchId(4711);
            result.Should().BeEmpty();
        }

        [Test]
        public async Task AddAdds()
        {
            var sut = this.mock.Create<SetService>();

            var set = new ResourceModel.Set
            {
                MatchId = 555,
                Number = 666,
                RedScore = 777,
                BlueScore = 888
            };
            await sut.Add(set);

            this.testSets.Should().Contain(m => set.IsEquivalent(m));
        }

        [Test]
        public async Task UpdateUpdates()
        {
            var sut = this.mock.Create<SetService>();

            var set = new ResourceModel.Set
            {
                MatchId = 1,
                Number = 1,
                RedScore = 6,
                BlueScore = 7
            };

            await sut.Update(set);

            var result = this.testSets.Single(s => s.MatchId == 1 && s.Number == 1);

            set.IsEquivalent(result).Should().BeTrue();
        }

        [Test]
        public async Task UpdateAddsChangeEntry()
        {
            var sut = this.mock.Create<SetService>();

            var oldEntry = Mapper.Map<Set, SetChange>(this.testSets[0]);

            var set = new ResourceModel.Set
            {
                MatchId = 1,
                Number = 1,
                RedScore = 42,
                BlueScore = 43
            };

            await sut.Update(set);

            var result = this.testSetChanges.Single(s => s.MatchId == 1 && s.Number == 1);
            oldEntry.ChangeTimeStamp = result.ChangeTimeStamp; // this property is obviously not the same because it is only present on MatchChange objects

            result.Should().BeEquivalentTo(oldEntry);
        }

        [Test]
        public void UpdateOfUnknownSetThrows()
        {
            var sut = this.mock.Create<SetService>();

            var set = new ResourceModel.Set
            {
                MatchId = 666,
                Number = 777,
                RedScore = 6,
                BlueScore = 7
            };

            Func<Task> act = async () => { await sut.Update(set); };
            act.Should().Throw<KeyNotFoundException>();
        }

        [Test]
        public async Task DeleteDeletes()
        {
            var sut = this.mock.Create<SetService>();
            const int MatchId = 1;
            const int ExistingNumber = 1;

            // delete an existing entry
            await sut.Delete(MatchId, ExistingNumber);
            this.testSets.Should().NotContain(p => p.MatchId == MatchId && p.Number == ExistingNumber && !p.IsDeleted);

            // deleting an entry twice
            await sut.Delete(MatchId, ExistingNumber);
            this.testSets.Should().NotContain(p => p.MatchId == MatchId && p.Number == ExistingNumber && !p.IsDeleted);

            // deleting a non-existing entry
            const int InvalidNumber = 42;
            await sut.Delete(MatchId, InvalidNumber);
            this.testSets.Should().NotContain(p => p.MatchId == MatchId && p.Number == ExistingNumber && !p.IsDeleted);
        }

        /// <summary>
        /// Deletings the completed fails.
        /// </summary>
        [Test]
        public void DeletingCompletedFails()
        {
            var sut = this.mock.Create<SetService>();
            const int MatchId = 3;
            const int Number = 1;

            Assert.ThrowsAsync<ActionNotSupportedException>(() => sut.Delete(MatchId, Number));
        }
    }
}
