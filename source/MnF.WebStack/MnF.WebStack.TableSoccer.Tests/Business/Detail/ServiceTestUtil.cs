﻿//-----------------------------------------------------------------------
// <copyright file="ServiceTestUtil.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Utility Class for service tests
    /// </summary>
    public static class ServiceTestUtil
    {
        /// <summary>
        /// Matcheses the equivalent.
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="domain">The domain.</param>
        /// <returns>Whether the matches are equivalent</returns>
        public static bool IsEquivalent(this MatchWrite resource, DomainModel.Match domain)
        {
            return ((resource == null && domain == null) || (resource != null && domain != null)) &&
                   resource.Id == domain.Id &&
                   resource.TimeStamp == domain.TimeStamp &&
                   resource.BluePlayerOneId == domain.BluePlayerOneId &&
                   resource.BluePlayerTwoId == domain.BluePlayerTwoId &&
                   resource.RedPlayerOneId == domain.RedPlayerOneId &&
                   resource.RedPlayerTwoId == domain.RedPlayerTwoId;
        }

        /// <summary>
        /// Checks whether the matches are equivalent
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="domain">The domain.</param>
        /// <returns>Whether the sets are equivalent</returns>
        public static bool IsEquivalent(this Set resource, DomainModel.Set domain)
        {
            return ((resource == null && domain == null) || (resource != null && domain != null)) &&
                   resource.MatchId == domain.MatchId &&
                   resource.Number == domain.Number &&
                   resource.RedScore == domain.RedScore &&
                   resource.BlueScore == domain.BlueScore;
        }
    }
}
