﻿//-----------------------------------------------------------------------
// <copyright file="MatchServiceTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;

    using Autofac.Extras.Moq;

    using AutoMapper;

    using FluentAssertions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.DomainModel;
    using MnF.WebStack.TableSoccer.ResourceModel;

    using Moq;

    using NUnit.Framework;

    using Match = MnF.WebStack.TableSoccer.DomainModel.Match;
    using Player = MnF.WebStack.TableSoccer.DomainModel.Player;

    /// <summary>
    /// Unit tests for the <see cref="MatchService"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class MatchServiceTests
    {
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();
        private static readonly IMapper Mapper = new MapperConfiguration(cfg => cfg.AddProfiles(typeof(MatchService))).CreateMapper();

        private AutoMock mock;

        private List<Match> testMatches;
        private List<MatchChange> testMatchChanges;

        private Mock<DbSet<Match>> mockMatchDbSet;
        private Mock<DbSet<MatchChange>> mockMatchChangeDbSet;

        private static IEnumerable<Match> TestMatches
        {
            get
            {
                var id = 1;
                yield return new Match
                {
                    Id = id++,
                    TimeStamp = Now,
                    RedPlayerOneId = 1,
                    RedPlayerTwoId = 2,
                    BluePlayerOneId = 3,
                    BluePlayerTwoId = 4,
                    RedPlayerOne = new Player { Id = 1 },
                    RedPlayerTwo = new Player { Id = 2 },
                    BluePlayerOne = new Player { Id = 3 },
                    BluePlayerTwo = new Player { Id = 4 }
                };

                yield return new Match
                {
                    Id = id++,
                    TimeStamp = Now,
                    RedPlayerOneId = 2,
                    RedPlayerTwoId = 3,
                    BluePlayerOneId = 4,
                    BluePlayerTwoId = 5,
                    RedPlayerOne = new Player { Id = 2 },
                    RedPlayerTwo = new Player { Id = 3 },
                    BluePlayerOne = new Player { Id = 4 },
                    BluePlayerTwo = new Player { Id = 5 }
                };

                yield return new Match
                {
                    Id = id,
                    TimeStamp = Now,
                    RedPlayerOneId = 3,
                    RedPlayerTwoId = 4,
                    BluePlayerOneId = 5,
                    BluePlayerTwoId = 6,
                    RedPlayerOne = new Player { Id = 3 },
                    RedPlayerTwo = new Player { Id = 4 },
                    BluePlayerOne = new Player { Id = 5 },
                    BluePlayerTwo = new Player { Id = 6 },
                    IsCompleted = true
                };
            }
        }

        private static IEnumerable<MatchChange> TestMatchChanges
        {
            get
            {
                yield return new MatchChange
                {
                    Id = 1,
                    MatchId = 42,
                    TimeStamp = Now,
                    RedPlayerOneId = 1,
                    RedPlayerTwoId = 2,
                    BluePlayerOneId = 3,
                    BluePlayerTwoId = 4
                };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testMatches = TestMatches.ToList();
            this.testMatchChanges = TestMatchChanges.ToList();

            // provide an AutoMapper configuration, loaded with the profiles from the assembly-under-test.
            this.mock.Provide<IConfigurationProvider>(new MapperConfiguration(cfg => cfg.AddProfiles(typeof(MatchService))));

            // provide a match repository containing the test matches
            this.mockMatchDbSet = new Mock<DbSet<Match>>().SetupData(this.testMatches);
            this.mock.Mock<IRepository<Match>>().SetupGet(r => r.Entities).Returns(this.mockMatchDbSet.Object);

            // provide a match repository containing the test match-changes
            this.mockMatchChangeDbSet = new Mock<DbSet<MatchChange>>().SetupData(this.testMatchChanges);
            this.mock.Mock<IRepository<MatchChange>>().SetupGet(r => r.Entities).Returns(this.mockMatchChangeDbSet.Object);
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetAllFromEmptyDatabase()
        {
            var localMockDbSet = new Mock<DbSet<Match>>().SetupData(new List<Match>());
            this.mock.Mock<IRepository<Match>>().SetupGet(r => r.Entities).Returns(localMockDbSet.Object);

            var sut = this.mock.Create<MatchService>();
            (await sut.GetAll()).Should().BeEmpty();
        }

        [Test]
        public async Task GetAllGetsAll()
        {
            var sut = this.mock.Create<MatchService>();
            (await sut.GetAll()).Count().Should().Be(3);
        }

        [Test]
        public async Task GetByIdSuccess()
        {
            var sut = this.mock.Create<MatchService>();
            var result = await sut.GetById(3);

            var matches = TestMatches.ToArray();
            var matchToCompare = Mapper.Map<Match, MatchRead>(matches[2]);

            result.Should().BeEquivalentTo(matchToCompare);
        }

        [Test]
        public async Task GetByIdReturnsNullIfIdIsUnknown()
        {
            var sut = this.mock.Create<MatchService>();
            (await sut.GetById(4711)).Should().BeNull();
        }

        [Test]
        public async Task AddAdds()
        {
            var sut = this.mock.Create<MatchService>();

            var match = new MatchWrite
            {
                TimeStamp = Now,
                RedPlayerOneId = 4444,
                RedPlayerTwoId = 5555,
                BluePlayerOneId = 6666,
                BluePlayerTwoId = 7777
            };
            await sut.Add(match);

            this.testMatches.Should().Contain(m => match.IsEquivalent(m));
        }

        [Test]
        public async Task AddSetsTheMatchesId()
        {
            this.mockMatchDbSet.Setup(d => d.Add(It.IsAny<Match>()))
                .Returns((Match m) =>
                    {
                        m.Id = 31415;
                        return m;
                    });

            var sut = this.mock.Create<MatchService>();
            var match = new MatchWrite
            {
                Id = 42,
                TimeStamp = Now,
                RedPlayerOneId = 4,
                RedPlayerTwoId = 3,
                BluePlayerOneId = 2,
                BluePlayerTwoId = 1
            };
            await sut.Add(match);

            match.Id.Should().Be(31415);
        }

        [Test]
        public async Task UpdateUpdates()
        {
            var sut = this.mock.Create<MatchService>();

            var match = new MatchWrite
            {
                Id = 2,
                TimeStamp = Now,
                RedPlayerOneId = 4,
                RedPlayerTwoId = 3,
                BluePlayerOneId = 2,
                BluePlayerTwoId = 1
            };

            await sut.Update(match);

            var result = this.testMatches.Single(p => p.Id == 2);

            match.IsEquivalent(result).Should().BeTrue();
        }

        [Test]
        public async Task UpdateAddsChangeEntry()
        {
            var sut = this.mock.Create<MatchService>();

            var oldEntry = Mapper.Map<Match, MatchChange>(this.testMatches[0]);

            var match = new MatchWrite
            {
                Id = 1,
                TimeStamp = Now,
                RedPlayerOneId = 9,
                RedPlayerTwoId = 9,
                BluePlayerOneId = 9,
                BluePlayerTwoId = 9
            };

            await sut.Update(match);

            var result = this.testMatchChanges.Single(p => p.MatchId == 1);
            oldEntry.ChangeTimeStamp = result.ChangeTimeStamp; // this property is obviously not the same because it is only present on MatchChange objects

            result.Should().BeEquivalentTo(oldEntry);
        }

        [Test]
        public void UpdateOfUnknownMatchThrows()
        {
            var sut = this.mock.Create<MatchService>();

            var match = new MatchWrite
            {
                Id = 666,
                TimeStamp = Now,
                RedPlayerOneId = 4,
                RedPlayerTwoId = 3,
                BluePlayerOneId = 2,
                BluePlayerTwoId = 1
            };

            Func<Task> act = async () => { await sut.Update(match); };
            act.Should().Throw<KeyNotFoundException>();
        }

        [Test]
        public async Task DeleteDeletes()
        {
            var sut = this.mock.Create<MatchService>();
            const int ExistingId = 1;

            // delete an existing entry
            await sut.Delete(ExistingId);
            this.testMatches.Should().NotContain(p => p.Id == ExistingId && !p.IsDeleted);

            // deleting an entry twice
            await sut.Delete(ExistingId);
            this.testMatches.Should().NotContain(p => p.Id == ExistingId && !p.IsDeleted);

            // deleting a non-existing entry
            const int InvalidId = 42;
            await sut.Delete(InvalidId);
            this.testMatches.Should().NotContain(p => p.Id == InvalidId && !p.IsDeleted);
        }

        /// <summary>
        /// Deletings the completed fails.
        /// </summary>
        [Test]
        public void DeletingCompletedFails()
        {
            var sut = this.mock.Create<MatchService>();
            const int Id = 3;

            Assert.ThrowsAsync<ActionNotSupportedException>(() => sut.Delete(Id));
        }
    }
}
