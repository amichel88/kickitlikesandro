﻿//-----------------------------------------------------------------------
// <copyright file="PlayerServiceTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using Autofac.Extras.Moq;

    using AutoMapper;

    using FluentAssertions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.DomainModel;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="PlayerService"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class PlayerServiceTests
    {
        private AutoMock mock;
        private List<Player> testPlayers;
        private Mock<DbSet<Player>> mockDbSet;

        private static IEnumerable<Player> TestPlayers
        {
            get
            {
                var id = 1;
                yield return new Player { Id = id++, FirstName = "Alain", LastName = "Berset", };
                yield return new Player { Id = id++, FirstName = "Ueli", LastName = "Maurer", };
                yield return new Player { Id = id++, FirstName = "Doris", LastName = "Leuthard", };
                yield return new Player { Id = id++, FirstName = "Simonetta", LastName = "Sommaruga", };
                yield return new Player { Id = id++, FirstName = "Johann", LastName = "Schneider-Ammann", };
                yield return new Player { Id = id++, FirstName = "Guy", LastName = "Parmelin", };
                yield return new Player { Id = id, FirstName = "Ignazio", LastName = "Cassis", };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testPlayers = TestPlayers.ToList();

            // provide an AutoMapper configuration, loaded with the profiles from the assembly-under-test.
            this.mock.Provide<IConfigurationProvider>(new MapperConfiguration(cfg => cfg.AddProfiles(typeof(PlayerService))));

            // provide a player repository containing the test players
            this.mockDbSet = new Mock<DbSet<Player>>().SetupData(this.testPlayers);
            this.mock.Mock<IRepository<Player>>().SetupGet(r => r.Entities).Returns(this.mockDbSet.Object);
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetAllFromEmptyDatabase()
        {
            var localMockDbSet = new Mock<DbSet<Player>>().SetupData(new List<Player>());
            this.mock.Mock<IRepository<Player>>().SetupGet(r => r.Entities).Returns(localMockDbSet.Object);

            var sut = this.mock.Create<PlayerService>();
            (await sut.GetAll()).Should().BeEmpty();
        }

        [Test]
        public async Task GetAllGetsAll()
        {
            var sut = this.mock.Create<PlayerService>();
            (await sut.GetAll()).Count().Should().Be(7);
        }

        [Test]
        public async Task GetByIdSuccess()
        {
            var sut = this.mock.Create<PlayerService>();
            var result = await sut.GetById(6);
            result.Id.Should().Be(6);
            result.FirstName.Should().Be("Guy");
            result.LastName.Should().Be("Parmelin");
        }

        [Test]
        public async Task GetByIdReturnsNullIfIdIsUnknown()
        {
            var sut = this.mock.Create<PlayerService>();
            (await sut.GetById(4711)).Should().BeNull();
        }

        [Test]
        public async Task AddAdds()
        {
            var sut = this.mock.Create<PlayerService>();
            var firstName = "This Person";
            var lastName = "Is not yet in the database";

            var player = new ResourceModel.Player { FirstName = firstName, LastName = lastName };
            await sut.Add(player);

            this.testPlayers.Should().Contain(p => p.FirstName == firstName && p.LastName == lastName);
        }

        [Test]
        public async Task AddSetsThePlayersId()
        {
            this.mockDbSet.Setup(d => d.Add(It.IsAny<Player>()))
                .Returns((Player p) =>
                    {
                        p.Id = 31415;
                        return p;
                    });

            var sut = this.mock.Create<PlayerService>();
            var player = new ResourceModel.Player { Id = 42, FirstName = "Foo", LastName = "Bar" };
            await sut.Add(player);

            player.Id.Should().Be(31415);
        }

        [Test]
        public async Task UpdateUpdates()
        {
            var sut = this.mock.Create<PlayerService>();

            var player = new ResourceModel.Player { Id = 2, FirstName = "Ueli", LastName = "Der Knecht" };
            await sut.Update(player);

            this.testPlayers.Single(p => p.Id == 2).Should().BeEquivalentTo(player);
        }

        [Test]
        public void UpdateOfUnknownPlayerThrows()
        {
            var sut = this.mock.Create<PlayerService>();

            var player = new ResourceModel.Player { Id = 4711, FirstName = "Ueli", LastName = "Der Pächter" };

            Func<Task> act = async () => { await sut.Update(player); };
            act.Should().Throw<KeyNotFoundException>();
        }

        [Test]
        public async Task DeleteDeletes()
        {
            var sut = this.mock.Create<PlayerService>();
            const int ExistingId = 1;

            // delete an existing entry
            await sut.Delete(ExistingId);
            this.testPlayers.Should().NotContain(p => p.Id == ExistingId && !p.IsDeleted);

            // deleting an entry twice
            await sut.Delete(ExistingId);
            this.testPlayers.Should().NotContain(p => p.Id == ExistingId && !p.IsDeleted);

            // deleting a non-existing entry
            const int InvalidId = 42;
            await sut.Delete(InvalidId);
            this.testPlayers.Should().NotContain(p => p.Id == InvalidId && !p.IsDeleted);
        }
    }
}
