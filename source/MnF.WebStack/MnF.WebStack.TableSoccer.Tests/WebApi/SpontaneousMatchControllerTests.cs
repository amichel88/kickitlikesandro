﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatchControllerTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.WebApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Autofac.Extras.Moq;

    using FluentAssertions;

    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;
    using MnF.WebStack.WebApi.WebApi;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="MatchController"/> class.
    /// </summary>
    internal sealed class SpontaneousMatchControllerTests
    {
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();
        private AutoMock mock;

        private static IEnumerable<SpontaneousMatch> TestMatches
        {
            get
            {
                yield return new SpontaneousMatch
                                 {
                                     Id = 10,
                                     TimeStampCreated = Now.Subtract(TimeSpan.FromMinutes(1)),
                                     ScoreBlue = 1,
                                     ScoreRed = 7,
                                 };

                yield return new SpontaneousMatch
                                 {
                                     Id = 11,
                                     TimeStampCreated = Now,
                                     ScoreBlue = 4,
                                     ScoreRed = 7,
                                 };

                yield return new SpontaneousMatch
                                 {
                                     Id = 20,
                                     TimeStampCreated = Now.Subtract(TimeSpan.FromHours(2)),
                                     ScoreBlue = 7,
                                     ScoreRed = 4,
                                     TimeStampCompleted = Now
                                 };

                yield return new SpontaneousMatch
                                 {
                                     Id = 30,
                                     TimeStampCreated = Now.Subtract(TimeSpan.FromHours(3)),
                                     ScoreBlue = 7,
                                     ScoreRed = 5,
                                     TimeStampCompleted = Now.Subtract(TimeSpan.FromMinutes(5))
                                 };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();

            // provide a match service serving the test matches
            this.mock.Mock<ISpontaneousMatchService>().Setup(s => s.GetCurrentMatch())
                .Returns(Task.FromResult(TestMatches.ToList()[1]));
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetCurrentSuccess()
        {
            var sut = this.mock.Create<SpontaneousMatchController>();
            var result = await sut.GetCurrentMatch();
            result.Should().BeEquivalentTo(TestMatches.ToList()[1]);
        }
    }
}
