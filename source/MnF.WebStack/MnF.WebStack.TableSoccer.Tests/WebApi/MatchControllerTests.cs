﻿//-----------------------------------------------------------------------
// <copyright file="MatchControllerTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.WebApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http.Results;

    using Autofac.Extras.Moq;

    using FluentAssertions;

    using MnF.WebStack.Common.Utility;
    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;
    using MnF.WebStack.WebApi.WebApi;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="MatchController"/> class.
    /// </summary>
    internal sealed class MatchControllerTests
    {
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();
        private AutoMock mock;

        private static IEnumerable<MatchRead> TestMatches
        {
            get
            {
                var id = 1;
                yield return new MatchRead
                {
                    Id = id++,
                    TimeStamp = Now,
                    RedPlayerOne = new Player { Id = 1 },
                    RedPlayerTwo = new Player { Id = 2 },
                    BluePlayerOne = new Player { Id = 3 },
                    BluePlayerTwo = new Player { Id = 4 },
                };

                yield return new MatchRead
                {
                    Id = id++,
                    TimeStamp = Now,
                    RedPlayerOne = new Player { Id = 2 },
                    RedPlayerTwo = new Player { Id = 3 },
                    BluePlayerOne = new Player { Id = 4 },
                    BluePlayerTwo = new Player { Id = 5 },
                };

                yield return new MatchRead
                {
                    Id = id,
                    TimeStamp = Now,
                    RedPlayerOne = new Player { Id = 3 },
                    RedPlayerTwo = new Player { Id = 4 },
                    BluePlayerOne = new Player { Id = 5 },
                    BluePlayerTwo = new Player { Id = 6 },
                };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();

            // provide a match service serving the test matches
            this.mock.Mock<IMatchService>().Setup(s => s.GetAll())
                .Returns(Task.FromResult(TestMatches));

            this.mock.Mock<IMatchService>().Setup(s => s.GetById(It.IsAny<int>()))
                .Returns((int i) => Task.FromResult(TestMatches.FirstOrDefault(p => p.Id == i)));
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetAllSuccess()
        {
            var sut = this.mock.Create<MatchController>();
            var result = await sut.GetAll();
            result.Should().BeEquivalentTo(TestMatches);
        }

        [Test]
        public async Task GetByIdSuccess()
        {
            var sut = this.mock.Create<MatchController>();
            var result = await sut.GetById(2) as OkNegotiatedContentResult<MatchRead>;
            result.Should().NotBeNull();

            var matches = TestMatches.ToArray();

            // ReSharper disable once PossibleNullReferenceException
            result.Content.Should().BeEquivalentTo(matches[1]);
        }

        [Test]
        public async Task GetByIdNotFound()
        {
            var sut = this.mock.Create<MatchController>();
            (await sut.GetById(4711))
                .Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task PostAddsToService()
        {
            var sut = this.mock.Create<MatchController>();

            var match = new MatchWrite
            {
                TimeStamp = Now,
                RedPlayerOneId = 7,
                RedPlayerTwoId = 8,
                BluePlayerOneId = 9,
                BluePlayerTwoId = 10,
            };

            await sut.PostNew(match);

            this.mock.Mock<IMatchService>().Verify(s => s.Add(It.Is((MatchWrite m) => m.Equals(match))), Times.Once);
        }

        [Test]
        public async Task PostSuccessReturnsCreated()
        {
            var sut = this.mock.Create<MatchController>();
            var match = new MatchWrite
            {
                TimeStamp = Now,
                RedPlayerOneId = 7,
                RedPlayerTwoId = 8,
                BluePlayerOneId = 9,
                BluePlayerTwoId = 10,
            };

            (await sut.PostNew(match))
                .Should().BeOfType<CreatedAtRouteNegotiatedContentResult<MatchWrite>>();
        }

        [Test]
        public async Task PutUpdatesToService()
        {
            var sut = this.mock.Create<MatchController>();

            var match = new MatchWrite
            {
                Id = 4713,
                TimeStamp = Now,
                RedPlayerOneId = 11,
                RedPlayerTwoId = 12,
                BluePlayerOneId = 13,
                BluePlayerTwoId = 14,
            };

            await sut.PutUpdate(match);

            this.mock.Mock<IMatchService>().Verify(s => s.Update(It.Is((MatchWrite m) => m.Equals(match))), Times.Once);
        }

        [Test]
        public async Task PutSuccessReturnsOk()
        {
            var sut = this.mock.Create<MatchController>();
            var match = new MatchWrite
            {
                Id = 4711,
                TimeStamp = Now,
                RedPlayerOneId = 7,
                RedPlayerTwoId = 8,
                BluePlayerOneId = 9,
                BluePlayerTwoId = 11,
            };

            var result = await sut.PutUpdate(match);
            result.Should().BeOfType<OkNegotiatedContentResult<MatchWrite>>();
        }

        [Test]
        public async Task PutUnknownReturnsNotFound()
        {
            var sut = this.mock.Create<MatchController>();

            this.mock.Mock<IMatchService>().Setup(s => s.Update(It.IsAny<MatchWrite>())).Throws<KeyNotFoundException>();

            var match = new MatchWrite
            {
                Id = 4711,
                TimeStamp = DateTime.Now.ForceToUtc(),
                RedPlayerOneId = 7,
                RedPlayerTwoId = 8,
                BluePlayerOneId = 9,
                BluePlayerTwoId = 10,
            };

            var result = await sut.PutUpdate(match);
            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task DeleteCallsService()
        {
            const int Id = 42;
            var sut = this.mock.Create<MatchController>();
            await sut.Delete(Id);

            this.mock.Mock<IMatchService>()
                .Verify(s => s.Delete(It.Is((int id) => id == Id)), Times.Once);
        }
    }
}
