//-----------------------------------------------------------------------
// <copyright file="201805231410013_AddSetChanges.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// adds a history-table documenting all changes on sets
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class AddSetChanges : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.SetChanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MatchId = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        OldRedScore = c.Int(nullable: false),
                        OldBlueScore = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Matches", t => t.MatchId)
                .Index(t => t.MatchId);
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.SetChanges", "MatchId", "dbo.Matches");
            this.DropIndex("dbo.SetChanges", new[] { "MatchId" });
            this.DropTable("dbo.SetChanges");
        }
    }
}
