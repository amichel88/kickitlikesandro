//-----------------------------------------------------------------------
// <copyright file="201805250721033_MakePlayerTwoIdsNullable.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Allow the second players to be null which indicates a 1 vs 1 match
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class MakePlayerTwoIdsNullable : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.DropIndex("dbo.Matches", new[] { "RedPlayerTwoId" });
            this.DropIndex("dbo.Matches", new[] { "BluePlayerTwoId" });
            this.AlterColumn("dbo.Matches", "RedPlayerTwoId", c => c.Int());
            this.AlterColumn("dbo.Matches", "BluePlayerTwoId", c => c.Int());
            this.CreateIndex("dbo.Matches", "RedPlayerTwoId");
            this.CreateIndex("dbo.Matches", "BluePlayerTwoId");
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropIndex("dbo.Matches", new[] { "BluePlayerTwoId" });
            this.DropIndex("dbo.Matches", new[] { "RedPlayerTwoId" });
            this.AlterColumn("dbo.Matches", "BluePlayerTwoId", c => c.Int(nullable: false));
            this.AlterColumn("dbo.Matches", "RedPlayerTwoId", c => c.Int(nullable: false));
            this.CreateIndex("dbo.Matches", "BluePlayerTwoId");
            this.CreateIndex("dbo.Matches", "RedPlayerTwoId");
        }
    }
}
