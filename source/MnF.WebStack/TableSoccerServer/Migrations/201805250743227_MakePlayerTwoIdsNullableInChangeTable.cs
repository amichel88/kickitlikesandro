//-----------------------------------------------------------------------
// <copyright file="201805250743227_MakePlayerTwoIdsNullableInChangeTable.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Make the second-player ids nullable in the change-history table as well
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class MakePlayerTwoIdsNullableInChangeTable : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.DropIndex("dbo.MatchChanges", new[] { "OldRedPlayerTwoId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldBluePlayerTwoId" });
            this.AlterColumn("dbo.MatchChanges", "OldRedPlayerTwoId", c => c.Int());
            this.AlterColumn("dbo.MatchChanges", "OldBluePlayerTwoId", c => c.Int());
            this.CreateIndex("dbo.MatchChanges", "OldRedPlayerTwoId");
            this.CreateIndex("dbo.MatchChanges", "OldBluePlayerTwoId");
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropIndex("dbo.MatchChanges", new[] { "OldBluePlayerTwoId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldRedPlayerTwoId" });
            this.AlterColumn("dbo.MatchChanges", "OldBluePlayerTwoId", c => c.Int(nullable: false));
            this.AlterColumn("dbo.MatchChanges", "OldRedPlayerTwoId", c => c.Int(nullable: false));
            this.CreateIndex("dbo.MatchChanges", "OldBluePlayerTwoId");
            this.CreateIndex("dbo.MatchChanges", "OldRedPlayerTwoId");
        }
    }
}
