//-----------------------------------------------------------------------
// <copyright file="201805301434438_RenamePlayerElo.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Renames the column for elo-count
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class RenamePlayerElo : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.AddColumn("dbo.Players", "EloCount", c => c.Int(nullable: false));
            this.DropColumn("dbo.Players", "elo");
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.AddColumn("dbo.Players", "elo", c => c.Int(nullable: false));
            this.DropColumn("dbo.Players", "EloCount");
        }
    }
}
