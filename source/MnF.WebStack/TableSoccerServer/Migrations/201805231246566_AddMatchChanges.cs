//-----------------------------------------------------------------------
// <copyright file="201805231246566_AddMatchChanges.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// adds a history-table documenting all changes on matches
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class AddMatchChanges : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.MatchChanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MatchId = c.Int(nullable: false),
                        OldTimeStamp = c.DateTime(nullable: false),
                        OldRedPlayerOneId = c.Int(nullable: false),
                        OldRedPlayerTwoId = c.Int(nullable: false),
                        OldBluePlayerOneId = c.Int(nullable: false),
                        OldBluePlayerTwoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Matches", t => t.MatchId)
                .Index(t => t.MatchId);
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.MatchChanges", "MatchId", "dbo.Matches");
            this.DropIndex("dbo.MatchChanges", new[] { "MatchId" });
            this.DropTable("dbo.MatchChanges");
        }
    }
}
