//-----------------------------------------------------------------------
// <copyright file="201805231432089_MatchChangesAddPlayerFKs.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Add foreign key constraints to player-ids on matches
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class MatchChangesAddPlayerFKs : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.CreateIndex("dbo.MatchChanges", "OldRedPlayerOneId");
            this.CreateIndex("dbo.MatchChanges", "OldRedPlayerTwoId");
            this.CreateIndex("dbo.MatchChanges", "OldBluePlayerOneId");
            this.CreateIndex("dbo.MatchChanges", "OldBluePlayerTwoId");
            this.AddForeignKey("dbo.MatchChanges", "OldBluePlayerOneId", "dbo.Players", "Id");
            this.AddForeignKey("dbo.MatchChanges", "OldBluePlayerTwoId", "dbo.Players", "Id");
            this.AddForeignKey("dbo.MatchChanges", "OldRedPlayerOneId", "dbo.Players", "Id");
            this.AddForeignKey("dbo.MatchChanges", "OldRedPlayerTwoId", "dbo.Players", "Id");
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.MatchChanges", "OldRedPlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.MatchChanges", "OldRedPlayerOneId", "dbo.Players");
            this.DropForeignKey("dbo.MatchChanges", "OldBluePlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.MatchChanges", "OldBluePlayerOneId", "dbo.Players");
            this.DropIndex("dbo.MatchChanges", new[] { "OldBluePlayerTwoId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldBluePlayerOneId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldRedPlayerTwoId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldRedPlayerOneId" });
        }
    }
}
