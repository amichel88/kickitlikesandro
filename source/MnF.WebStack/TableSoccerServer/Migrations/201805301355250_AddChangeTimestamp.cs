//-----------------------------------------------------------------------
// <copyright file="201805301355250_AddChangeTimestamp.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Adds columns to the change-tables to store the change-date
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class AddChangeTimestamp : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.AddColumn("dbo.MatchChanges", "ChangeTimeStamp", c => c.DateTime(nullable: false));
            this.AddColumn("dbo.SetChanges", "ChangeTimeStamp", c => c.DateTime(nullable: false));
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropColumn("dbo.SetChanges", "ChangeTimeStamp");
            this.DropColumn("dbo.MatchChanges", "ChangeTimeStamp");
        }
    }
}
