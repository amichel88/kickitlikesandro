﻿//-----------------------------------------------------------------------
// <copyright file="GeneralUtil.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.Utility
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Utility for miscellaneous functions
    /// </summary>
    public static class GeneralUtil
    {
        /// <summary>
        /// Determines whether the array is null or empty.
        /// </summary>
        /// <typeparam name="T">the generic type of the enumberable</typeparam>
        /// <param name="enumerable">The array.</param>
        /// <returns>
        ///   <c>true</c> if the array is null or empty [the specified array]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }
    }
}
