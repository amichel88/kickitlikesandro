﻿//-----------------------------------------------------------------------
// <copyright file="AutofacValidatorFactory.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.WebApi.Validation
{
    using System;
    using Autofac;
    using FluentValidation;

    /// <summary>
    /// Validator factory to be used with Autofac
    /// </summary>
    /// <seealso cref="FluentValidation.ValidatorFactoryBase" />
    public class AutofacValidatorFactory : ValidatorFactoryBase
    {
        private readonly IComponentContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutofacValidatorFactory"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public AutofacValidatorFactory(IComponentContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Instantiates the validator
        /// </summary>
        /// <param name="validatorType">The type of the validator to instantiate</param>
        /// <returns>the instantiated validator</returns>
        public override IValidator CreateInstance(Type validatorType)
        {
            if (this.context.TryResolve(validatorType, out var instance))
            {
                var validator = instance as IValidator;
                return validator;
            }

            return null;
        }
    }
}
