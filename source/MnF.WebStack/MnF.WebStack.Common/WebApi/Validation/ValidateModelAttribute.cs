﻿//-----------------------------------------------------------------------
// <copyright file="ValidateModelAttribute.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.WebApi.Validation
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    using FluentValidation;
    using FluentValidation.Results;

    /// <summary>
    /// Filter attribute validating the incoming resources.
    /// </summary>
    public sealed class ValidateModelAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Occurs before the action method is invoked.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid)
            {
                actionContext = RunValidators(actionContext).Result;
            }

            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest,
                    actionContext.ModelState);
            }
        }

        private static async Task<HttpActionContext> RunValidators(HttpActionContext actionContext)
        {
            var validationTasks = new List<Task<ValidationResult>>();

            foreach (var argument in actionContext.ActionArguments)
            {
                var value = argument.Value;
                var type = value?.GetType();
                var isClass = type?.IsClass ?? false;

                if (!isClass)
                {
                    continue;
                }

                var validator = (IValidator)actionContext
                    .Request
                    .GetDependencyScope()
                    .GetService(typeof(IValidator<>)
                    .MakeGenericType(type));

                if (validator == null)
                {
                    continue;
                }

                if (validator is ITsValidator tsValidator)
                {
                    tsValidator.SetHttpActionContext(actionContext);
                }

                validationTasks.Add(validator.ValidateAsync(value));
            }

            var validationResults = await Task.WhenAll(validationTasks);

            foreach (var validationResult in validationResults)
            {
                if (validationResult.IsValid)
                {
                    continue;
                }

                foreach (var error in validationResult.Errors)
                {
                    actionContext.ModelState.AddModelError(error.ErrorCode, error.ErrorMessage);
                }
            }

            return actionContext;
        }
    }
}
