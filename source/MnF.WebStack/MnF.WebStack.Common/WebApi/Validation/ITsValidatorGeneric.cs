﻿//-----------------------------------------------------------------------
// <copyright file="ITsValidatorGeneric.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.WebApi.Validation
{
    using FluentValidation;

    /// <summary>
    /// Common behaviour for validators: Interface with generics to offer type-safety
    /// </summary>
    /// <typeparam name="T">The type to be validated</typeparam>
    /// <seealso cref="FluentValidation.IValidator{T}" />
    /// <seealso cref="FluentValidation.IValidator" />
    public interface ITsValidatorGeneric<in T> : IValidator<T>, ITsValidator
    {
    }
}
