﻿//-----------------------------------------------------------------------
// <copyright file="ITsValidator.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.WebApi.Validation
{
    using System.Web.Http.Controllers;

    using FluentValidation;

    /// <summary>
    /// Common behaviour for validators: Interface without generics to make casts easier.
    /// </summary>
    /// <seealso cref="FluentValidation.IValidator" />
    public interface ITsValidator : IValidator
    {
        /// <summary>
        /// Sets the HTTP action context.
        /// </summary>
        /// <param name="context">The context.</param>
        void SetHttpActionContext(HttpActionContext context);
    }
}
