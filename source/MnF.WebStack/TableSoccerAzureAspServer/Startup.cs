﻿
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TableSoccerAzureAspServer.Startup))]
namespace TableSoccerAzureAspServer
{
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;
    using System.Collections.Generic;

    using Autofac;
    using Autofac.Integration.WebApi;

    using AutoMapper;

    using FluentValidation.WebApi;

    using MnF.WebStack.Common.WebApi;
    using MnF.WebStack.Common.WebApi.Validation;

    using Newtonsoft.Json.Serialization;

    public class Startup
    {
        private readonly IContainer autofacContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        public Startup()
        {
            this.autofacContainer = ConfigureAutofac();
        }

        /// <summary>
        /// Gets assemblies of the required components.
        /// </summary>
        public static IEnumerable<Assembly> RequiredComponents
        {
            get
            {
                yield return typeof(MnF.WebStack.TableSoccer.Business.IPlayerService).Assembly;
                yield return typeof(MnF.WebStack.WebApi.WebApi.PlayerController).Assembly;
            }
        }

        /// <summary>
        /// Configures the application on the specified application builder.
        /// </summary>
        /// <param name="appBuilder">The application builder.</param>
        public void Configuration(IAppBuilder appBuilder)
        {
            // This server will be accessed by clients from other domains, so we open up CORS.
            // This needs to be done before the call to .MapSignalR()!
            appBuilder.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            // As we use autofac, we have to register it first.
            // This enables having a lifetime scope per request.
            appBuilder.UseAutofacMiddleware(this.autofacContainer);

            this.ConfigureSignalR(appBuilder);
            this.ConfigureWebApi(appBuilder);

            ConfigureWebServer(appBuilder);
        }

        private static IContainer ConfigureAutofac()
        {
            var builder = new ContainerBuilder();

            // register the autofac dependency resolvers for WebApi and SignalR
            builder.RegisterType<AutofacWebApiDependencyResolver>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<Autofac.Integration.SignalR.AutofacDependencyResolver>()
                .AsImplementedInterfaces()
                .SingleInstance();

            // Register all autofac modules of the required components
            foreach (var component in RequiredComponents)
            {
                builder.RegisterAssemblyModules(component);
            }

            // Configure the auto mapper with the profiles in the required components
            // and register the configuration to autofac.
            var mapperConfig = new MapperConfiguration(cfg => cfg.AddProfiles(RequiredComponents));
            builder.RegisterInstance(mapperConfig)
                .AsImplementedInterfaces();

            // register the application specific implementations.
            builder.RegisterType<Persistence.TableSoccerRepository>()
                .AsImplementedInterfaces()
                .InstancePerRequest();

            return builder.Build();
        }


        private static void ConfigureWebServer(IAppBuilder appBuilder)
        {
            appBuilder.UseStaticFiles();
        }

        private void ConfigureSignalR(IAppBuilder appBuilder)
        {
            appBuilder.Map("/api/v1/signalr", map =>
                {
                    var hubConfiguration = new Microsoft.AspNet.SignalR.HubConfiguration
                                               {
                                                   Resolver = this.autofacContainer.Resolve<Microsoft.AspNet.SignalR.IDependencyResolver>(),
                                                   // EnableDetailedErrors = true,       // use this for debugging purposes but not for production
                                               };

                    map.RunSignalR(hubConfiguration);
                });
        }

        private void ConfigureWebApi(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();

            // Attribute routing.
            config.MapHttpAttributeRoutes();

            // Use autofac for the dependency resolving.
            var dependencyResolver = new AutofacWebApiDependencyResolver(this.autofacContainer);
            config.DependencyResolver = dependencyResolver;
            appBuilder.UseAutofacWebApi(config);

            // Translate between the naming conventions fro properties.
            // In C# properties are PascalCase, but in JavaScript they are camelCase.
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Log exceptions leaking out of the controllers.
            config.Services.Add(typeof(IExceptionLogger), new NLogExceptionLogger());

            // Add Filter
            config.Filters.Add(new ValidateModelAttribute());

            // Fluent Validation
            FluentValidationModelValidatorProvider.Configure(config);

            appBuilder.UseWebApi(config);
        }
    }
}
