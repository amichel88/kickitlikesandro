﻿namespace MnF.WebStack.AutofacOnAzureFunctions.Services.Modules
{
    using Autofac;

    using MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc;

    public class CommonModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ObjectResolver>().As<IObjectResolver>().SingleInstance();
        }
    }
}