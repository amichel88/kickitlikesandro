﻿namespace MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;

    using Microsoft.Azure.WebJobs.Host.Bindings;

    internal class InjectAttributeValueProvider : IValueProvider
    {
        private readonly ParameterInfo _parameterInfo;
        private readonly IObjectResolver _objectResolver;

        public InjectAttributeValueProvider(ParameterInfo parameterInfo, IObjectResolver objectResolver)
        {
            this._parameterInfo = parameterInfo;
            this._objectResolver = objectResolver;
        }

        public Task<object> GetValueAsync()
        {
            return Task.FromResult(this._objectResolver.Resolve(this.Type));
        }

        public string ToInvokeString()
        {
            return this.Type.ToString();
        }

        public Type Type => this._parameterInfo.ParameterType;
    }
}