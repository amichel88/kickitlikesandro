﻿namespace MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc
{
    using System.Reflection;
    using System.Threading.Tasks;

    using Microsoft.Azure.WebJobs.Host.Bindings;
    using Microsoft.Azure.WebJobs.Host.Protocols;

    public class InjectAttributeBinding : IBinding
    {
        private readonly ParameterInfo _parameterInfo;
        private readonly IObjectResolver _objectResolver;

        public InjectAttributeBinding(ParameterInfo parameterInfo, IObjectResolver objectResolver)
        {
            this._parameterInfo = parameterInfo;
            this._objectResolver = objectResolver;
        }

        public Task<IValueProvider> BindAsync(object value, ValueBindingContext context)
        {
            return Task.FromResult<IValueProvider>(new InjectAttributeValueProvider(this._parameterInfo, this._objectResolver));
        }

        public Task<IValueProvider> BindAsync(BindingContext context)
        {
            return Task.FromResult<IValueProvider>(new InjectAttributeValueProvider(this._parameterInfo, this._objectResolver));
        }

        public ParameterDescriptor ToParameterDescriptor()
        {
            return new ParameterDescriptor
            {
                Name = this._parameterInfo.Name,
                DisplayHints = new ParameterDisplayHints
                {
                    Description = "Inject services",
                    DefaultValue = "Inject services",
                    Prompt = "Inject services"
                }
            };
        }

        public bool FromAttribute => true;
    }
}