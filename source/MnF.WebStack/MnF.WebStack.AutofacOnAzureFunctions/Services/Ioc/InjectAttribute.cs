﻿namespace MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc
{
    using System;

    using Microsoft.Azure.WebJobs.Description;

    [AttributeUsage(AttributeTargets.Parameter)]
    [Binding]
    public class InjectAttribute : Attribute
    {
    }
}
