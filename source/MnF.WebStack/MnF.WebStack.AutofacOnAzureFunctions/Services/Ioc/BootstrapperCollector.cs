﻿namespace MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    internal class BootstrapperCollector
    {
        public List<Type> GetBootstrappers()
        {
            var exceptions = new List<System.Reflection.ReflectionTypeLoadException>();

            var result = AppDomain.CurrentDomain.GetAssemblies()
                .Where(assembly => assembly.FullName.StartsWith("MnF")) // TODO make this more generic! e.g. pass the prefix to filter for to the attribute
                .SelectMany(assembly =>
                    {
                        var types = new Type[0];
                        try
                        {
                            types = assembly.GetTypes();
                        }
                        catch (System.Reflection.ReflectionTypeLoadException e)
                        {
                            Console.WriteLine("Failed to get types of assembly " + assembly.FullName, e);
                            exceptions.Add(e);
                        }

                        return types;
                    })
                .Where(x => typeof(IBootstrapper).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .ToList();

            if (exceptions.Count > 0)
            {
                throw exceptions[0];
            }

            return result;
        }
    }
}
