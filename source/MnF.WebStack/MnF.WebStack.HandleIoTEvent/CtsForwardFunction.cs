using IoTHubTrigger = Microsoft.Azure.WebJobs.ServiceBus.EventHubTriggerAttribute;

using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.ServiceBus.Messaging;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc;
using MnF.WebStack.TableSoccer.Business;
using Newtonsoft.Json;
using System;

namespace MnF.WebStack.HandleIoTEvent
{
    public static class CtsForwardFunction
    {
        public class GoalSchema
        {
            public int goalBlue { get; set; }
            public int goalRed { get; set; }
        }
        public class ResetSchema
        {
            public bool reset { get; set; }
        }
        public class UndoSchema
        {
            public bool undo { get; set; }
        }

        private static HttpClient client = new HttpClient();

        [FunctionName("CtsForwardFunction")]
        public static async void Run([IoTHubTrigger("messages/events", Connection = "ctsiothub_events_IOTHUB")]EventData myEventHubMessage,
            TraceWriter log,
            [Inject]  ISpontaneousMatchService matchService)
        {
            log.Info($"Start decoding...");
            try
            {
                var jsonBody = Encoding.UTF8.GetString(myEventHubMessage.GetBytes());
                 
                var schema = myEventHubMessage.Properties["$$MessageSchema"];
                if (schema.Equals("GoalSchema"))
                {
                    GoalSchema myMessage = JsonConvert.DeserializeObject<GoalSchema>(jsonBody);
                    if (myMessage.goalBlue == 1)
                    {
                        log.Info($"incBlue");
                        await matchService.IncrementBlue();
                        await SignalChange();
                    }
                    if (myMessage.goalRed == 1)
                    {
                        log.Info($"incRed");
                        await matchService.IncrementRed();
                        await SignalChange();
                    }
                } else if (schema.Equals("ResetSchema"))
                {
                    ResetSchema myMessage = JsonConvert.DeserializeObject<ResetSchema>(jsonBody);
                    if (myMessage.reset)
                    {
                        log.Info($"reset");
                        await matchService.ResetMatch();
                        await SignalChange();
                    }
                } else if (schema.Equals("UndoSchema"))
                {
                    UndoSchema myMessage = JsonConvert.DeserializeObject<UndoSchema>(jsonBody);
                    if (myMessage.undo)
                    {
                        log.Info($"undo");
                        await matchService.UndoMatch();
                        await SignalChange();
                    }
                }
            }
            catch (Exception e)
            {
                log.Info($"Exception {e.Message}");
            }
        }
        private static async Task SignalChange()
        {
            var srHubConnection = new HubConnection("http://mfcloudkicker.azurewebsites.net//api/v1");
            var srHubProxy = srHubConnection.CreateHubProxy("TableSoccerHub");
            await srHubConnection.Start();
            await srHubProxy.Invoke("SignalModelChanged");
        }
    }
}
