﻿//-----------------------------------------------------------------------
// <copyright file="Bootstrapper.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.CurrentMatch
{
    using Autofac;

    using MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc;
    using MnF.WebStack.AzureFunctions.Common.AutofacInterface;

    /// <summary>
    /// Handles Autofac initialization. This class needs to be in the assembly of the Azure Function
    /// </summary>
    /// <seealso cref="MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc.IBootstrapper" />
    public class Bootstrapper: IBootstrapper
    {
        /// <summary>
        /// Creates the modules.
        /// </summary>
        /// <returns></returns>
        public Module[] CreateModules()
        {
            return new Module[] { new AutofacModule() };
        }
    }
}
