﻿//-----------------------------------------------------------------------
// <copyright file="AutofacModule.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.AzureFunctions.Common.AutofacInterface
{
    using System.Collections.Generic;
    using System.Reflection;

    using Autofac;
    using Autofac.Integration.WebApi;

    using AutoMapper;

    using MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc;
    using MnF.WebStack.TableSoccer.Business.Detail;

    using Module = Autofac.Module;

    /// <summary>
    /// Handles the Autofac dependency injection
    /// </summary>
    /// <seealso cref="Autofac.Module" />
    public class AutofacModule : Module
    {
        /// <summary>
        /// Gets assemblies of the required components.
        /// </summary>
        public static IEnumerable<Assembly> RequiredComponents
        {
            get
            {
                yield return typeof(TableSoccer.Business.IPlayerService).Assembly;
            }
        }

        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            // register the autofac dependency resolvers for WebApi and SignalR
            builder.RegisterType<AutofacWebApiDependencyResolver>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<Autofac.Integration.SignalR.AutofacDependencyResolver>()
                .AsImplementedInterfaces()
                .SingleInstance();

            // Register all autofac modules of the required components
            foreach (var component in RequiredComponents)
            {
                builder.RegisterAssemblyModules(component);
            }

            // Configure the auto mapper with the profiles in the required components
            // and register the configuration to autofac.
            var mapperConfig = new MapperConfiguration(cfg => cfg.AddProfiles(RequiredComponents));
            builder.RegisterInstance(mapperConfig)
                .AsImplementedInterfaces();

            // register the application specific implementations.
            builder.RegisterType<Persistence.TableSoccerRepository>()
                .AsImplementedInterfaces()
                ;

            builder.RegisterType<SpontaneousMatchService>()
                .AsImplementedInterfaces()
                ;
        }
    }

}
