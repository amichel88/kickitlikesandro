import { Component, OnInit } from '@angular/core';

import { SignalRService } from './signalr/signalr.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private signalrService: SignalRService) {}

  ngOnInit() {
    this.signalrService.start();
  }
}
