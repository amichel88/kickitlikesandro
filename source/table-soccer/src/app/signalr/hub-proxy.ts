import { NgZone } from '@angular/core';

type Handler = (arg: any) => void;

export interface HandlerRegistry {
  on(eventName: string, handler: Handler): void;
}

export abstract class HubProxy implements HandlerRegistry {

  readonly name: string;

  private handlers_: [string, Handler][] = [];
  private registry_: HandlerRegistry;
  private zone_: NgZone;

  protected constructor(name: string) {
    console.log(`SignalR: HubProxy(${name})`);

    this.name = name;
  }

  on(eventName: string, handler: Handler) {
    if (this.registry_ != null) {
      this.registry_.on(eventName, arg => this.zone_.run(() => handler(arg)));
    } else {
      this.handlers_.push([eventName, handler]);
    }
  }

  setHandlerRegistry(registry: HandlerRegistry, zone: NgZone) {
    this.registry_ = registry;
    this.zone_ = zone;
    this.handlers_.forEach(h => registry.on(h[0], arg => this.zone_.run(() => h[1](arg))));
  }

  onReconnected() {}

}
