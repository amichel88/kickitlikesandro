import { isDevMode } from '@angular/core';

export function getHostApiUrl(): string{
  const apiUrl = getHostUrl() + "/api/v1/";
  // console.log("API-URL: ", apiUrl);
  return apiUrl;
}

function getHostUrl(): string {
  if(isDevMode()){
    return "http://localhost:9000";
  }
  else {
    return window.location.protocol + "//" + window.location.host;
  }
}
