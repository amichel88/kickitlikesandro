import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { NgModule } from '@angular/core';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MainNavComponent } from './main-nav/main-nav.component';
import { PlayersModule } from './players/players.module';
import { HubProxy } from './signalr/hub-proxy';
import { TableSoccerService } from './table-soccer.service';
import {SpontaneousMatchesModule} from './spontaneous-matches/spontaneous-matches.module';
import { AdminModule } from './admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,

    PlayersModule,
    SpontaneousMatchesModule,
    AdminModule,

    AppRoutingModule, // must be last import
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: HubProxy, useExisting: TableSoccerService, multi: true },
  ]
})
export class AppModule { }
