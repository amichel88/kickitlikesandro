import { SpontaneousMatchesModule } from './spontaneous-matches.module';

describe('SpontaneousMatchesModule', () => {
  let spontaneousMatchesModule: SpontaneousMatchesModule;

  beforeEach(() => {
    spontaneousMatchesModule = new SpontaneousMatchesModule();
  });

  it('should create an instance', () => {
    expect(spontaneousMatchesModule).toBeTruthy();
  });
});
