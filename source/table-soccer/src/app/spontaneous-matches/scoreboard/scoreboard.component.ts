import { Component, OnInit } from '@angular/core';
import {SpontaneousMatchService} from '../spontaneous-match.service';
import {SpontaneousMatch} from '../spontaneousMatch';
import {startWith, switchMap} from 'rxjs/operators';
import {TableSoccerService} from '../../table-soccer.service';
import {Observable} from 'rxjs';
import _ from "lodash";
import * as moment from 'moment';

const ONE_SECOND_MS = 1000;
const ONE_MINUTE_MS = 60 * ONE_SECOND_MS;
const ONE_HOUR_MS = 60 * ONE_MINUTE_MS;
const ONE_DAY_MS = 24 * ONE_HOUR_MS;

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {
  currentMatch$: Observable<SpontaneousMatch>;

  constructor(
    private matchService: SpontaneousMatchService,
    private tableSoccerService: TableSoccerService
  ) { }

  ngOnInit() {
    this.subscribeCurrentMatch();
  }

  subscribeCurrentMatch() {
    this.currentMatch$ = this.tableSoccerService.modelChanged$.pipe(
      startWith(0),
      switchMap(() => this.matchService.getCurrentSpontaneousMatch()),
    );
  }

  getScore(match: SpontaneousMatch, propName: string): number {
    return _.get(match, propName, 0);
  }


  getGameTime(match: SpontaneousMatch): string{
    const timestamp = _.get(match, "timeStampCreated");

    if(_.isEmpty(timestamp)) {
      return null;
    }

    const timestampMoment = moment(timestamp);
    const timeDiffMs = moment().diff(timestampMoment);

    return this.formatTimeDifference(timeDiffMs);
  }

  getTimeUnit(timeDiffMs: number, msPerUnit: number): number {
    return Math.floor(timeDiffMs / msPerUnit);
  }

  formatTimeDifference(timeDiffMs: number): string {
    let remainingTimeDiffMs = timeDiffMs;

    const days = this.getTimeUnit(remainingTimeDiffMs, ONE_DAY_MS);
    remainingTimeDiffMs -= days * ONE_DAY_MS;

    const hours = this.getTimeUnit(remainingTimeDiffMs, ONE_HOUR_MS);
    remainingTimeDiffMs -= hours * ONE_HOUR_MS;

    const minutes = this.getTimeUnit(remainingTimeDiffMs, ONE_MINUTE_MS);
    remainingTimeDiffMs -= minutes * ONE_MINUTE_MS;

    const seconds = this.getTimeUnit(remainingTimeDiffMs, ONE_SECOND_MS);

    let result = '';
    let keepZeroValues = false;

    if (days > 0) {
      result += `${days} days, `;
      keepZeroValues = true;
    }
    if (keepZeroValues || hours > 0) {
      result += `${hours} hours, `;
      keepZeroValues = true;
    }
    if (keepZeroValues || minutes > 0) {
      result += `${minutes} minutes, `;
    }

    result += `${seconds} seconds`;
    return result;
  }
}
