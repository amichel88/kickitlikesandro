import { SpontaneousMatchRoutingModule } from './spontaneous-match-routing.module';

describe('SpontaneousMatchRoutingModule', () => {
  let spontaneousMatchRoutingModule: SpontaneousMatchRoutingModule;

  beforeEach(() => {
    spontaneousMatchRoutingModule = new SpontaneousMatchRoutingModule();
  });

  it('should create an instance', () => {
    expect(spontaneousMatchRoutingModule).toBeTruthy();
  });
});
