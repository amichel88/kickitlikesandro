import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import _ from "lodash";
import * as moment from 'moment';
import { HostListener } from '@angular/core';

import { AdminService } from '../admin.service';
import { TableSoccerService } from '../../table-soccer.service';
import { SpontaneousMatchService } from '../../spontaneous-matches/spontaneous-match.service';
import { SpontaneousMatch } from '../../spontaneous-matches/spontaneousMatch';

const ONE_SECOND_MS = 1000;
const ONE_MINUTE_MS = 60 * ONE_SECOND_MS;
const ONE_HOUR_MS = 60 * ONE_MINUTE_MS;
const ONE_DAY_MS = 24 * ONE_HOUR_MS;

@Component({
  selector: 'app-players-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '(document:keypress)': 'handleKeyboardEvent($event)'
  }
})
export class OverviewComponent implements OnInit {
  currentMatch$: Observable<SpontaneousMatch>;

  constructor(private adminService: AdminService,
    private matchService: SpontaneousMatchService,
    private tableSoccerService: TableSoccerService) {}

  ngOnInit() {
    this.subscribeCurrentMatch();
  }
  
  handleKeyboardEvent(event: KeyboardEvent) {
    console.log(event);
    if (event.key === ' ') {
      console.log('space -> undo');
      this.undo();
    }
    if (event.key === 'r') {
      console.log('r -> reset');
      this.reset();
    }
  }

  subscribeCurrentMatch() {
    this.currentMatch$ = this.tableSoccerService.modelChanged$.pipe(
      startWith(0),
      switchMap(() => this.matchService.getCurrentSpontaneousMatch()),
    );
  }

  getScore(match: SpontaneousMatch, propName: string): number {
    return _.get(match, propName, 0);
  }


  getGameTime(match: SpontaneousMatch): string{
    const timestamp = _.get(match, "timeStampCreated");

    if(_.isEmpty(timestamp)) {
      return null;
    }

    const timestampMoment = moment(timestamp);
    const timeDiffMs = moment().diff(timestampMoment);

    return this.formatTimeDifference(timeDiffMs);
  }

  getTimeUnit(timeDiffMs: number, msPerUnit: number): number {
    return Math.floor(timeDiffMs / msPerUnit);
  }

  formatTimeDifference(timeDiffMs: number): string {
    let remainingTimeDiffMs = timeDiffMs;

    const days = this.getTimeUnit(remainingTimeDiffMs, ONE_DAY_MS);
    remainingTimeDiffMs -= days * ONE_DAY_MS;

    const hours = this.getTimeUnit(remainingTimeDiffMs, ONE_HOUR_MS);
    remainingTimeDiffMs -= hours * ONE_HOUR_MS;

    const minutes = this.getTimeUnit(remainingTimeDiffMs, ONE_MINUTE_MS);
    remainingTimeDiffMs -= minutes * ONE_MINUTE_MS;

    const seconds = this.getTimeUnit(remainingTimeDiffMs, ONE_SECOND_MS);

    let result = '';
    let keepZeroValues = false;

    if (days > 0) {
      result += `${days} days, `;
      keepZeroValues = true;
    }
    if (keepZeroValues || hours > 0) {
      result += `${hours} hours, `;
      keepZeroValues = true;
    }
    if (keepZeroValues || minutes > 0) {
      result += `${minutes} minutes, `;
    }

    result += `${seconds} seconds`;
    return result;
  }

  reset() {
    this.adminService.reset().subscribe();
  }
  undo() {
    this.adminService.undo().subscribe();
  }
  incBlue() {
    this.adminService.incBlue().subscribe();
  }
  decBlue() {
    this.adminService.decBlue().subscribe();
  }
  incRed() {
    this.adminService.incRed().subscribe();
  }
  decRed() {
    this.adminService.decRed().subscribe();
  }
}

