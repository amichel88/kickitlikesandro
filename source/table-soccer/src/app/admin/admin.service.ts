import {Injectable, isDevMode} from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { delayIfDebug } from '../rx-operator';
import {getHostApiUrl} from '../util';

import {SpontaneousMatch} from '../spontaneous-matches/spontaneousMatch';
function getUrl() {
  return getHostApiUrl() + 'spontaneousMatch';  // TODO use environment variable instead!
}

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) {}

  reset(): Observable<SpontaneousMatch> {
    return this.http.post<SpontaneousMatch>(`${getUrl()}/${'reset'}`, null).pipe(
      delayIfDebug(500)
    );
  }

  undo(): Observable<SpontaneousMatch> {
    return this.http.post<SpontaneousMatch>(`${getUrl()}/${'undo'}`, null).pipe(
      delayIfDebug(500)
    );
  }

  incBlue(): Observable<SpontaneousMatch> {
    return this.http.post<SpontaneousMatch>(`${getUrl()}/${'incBlue'}`, null).pipe(
      delayIfDebug(500)
    );
  }

  decBlue(): Observable<SpontaneousMatch> {
    return this.http.post<SpontaneousMatch>(`${getUrl()}/${'decBlue'}`, null).pipe(
      delayIfDebug(500)
    );
  }
  
  incRed(): Observable<SpontaneousMatch> {
    return this.http.post<SpontaneousMatch>(`${getUrl()}/${'incRed'}`, null).pipe(
      delayIfDebug(500)
    );
  }

  decRed(): Observable<SpontaneousMatch> {
    return this.http.post<SpontaneousMatch>(`${getUrl()}/${'decRed'}`, null).pipe(
      delayIfDebug(500)
    );
  }
}
