import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { OverviewComponent } from './overview/overview.component';
import { AdminRoutingModule } from './admin-routing.module';

import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
} from '@angular/material';

@NgModule({
  declarations: [
    MainComponent,
    OverviewComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,

    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,

  ],
  exports: [
  ]
})
export class AdminModule { }
