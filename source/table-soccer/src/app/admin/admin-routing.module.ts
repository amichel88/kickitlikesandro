import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './main/main.component';
import { OverviewComponent } from './overview/overview.component';

const playersRoutes: Routes = [
  {
    path: 'admin',
    component: MainComponent,
    children: [
      { path: '', component: OverviewComponent },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(playersRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AdminRoutingModule { }
