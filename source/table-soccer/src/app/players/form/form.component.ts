import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Player } from '../player';

@Component({
  selector: 'app-players-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Input() player: Player;
  @Output() accept = new EventEmitter();
  @Output() cancel = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
