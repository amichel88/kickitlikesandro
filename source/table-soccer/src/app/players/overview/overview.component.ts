import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { PlayersService } from '../players.service';
import { Player } from '../player';
import { TableSoccerService } from '../../table-soccer.service';

@Component({
  selector: 'app-players-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  players$: Observable<Player[]>;

  constructor(private playersService: PlayersService, private tableSoccerService: TableSoccerService) {}

  ngOnInit() {
    this.players$ = this.tableSoccerService.modelChanged$.pipe(
      startWith(0),
      switchMap(() => this.playersService.getAll()),
      map(ps => ps.sort((l, r) => l.lastName.localeCompare(r.lastName))),
    );
  }

  delete(player: Player) {
    this.playersService.delete(player).subscribe();
  }

}
