import {Injectable, isDevMode} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Player } from './player';
import { delayIfDebug } from '../rx-operator';
import {getHostApiUrl} from '../util';

function getUrl(){
  return getHostApiUrl() + 'player';  // TODO use environment variable instead!
}

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor(private http: HttpClient) {}

  getAll(): Observable<Player[]> {
    return this.http.get<Player[]>(getUrl());
  }

  getById(id: number): Observable<Player> {
    return this.http.get<Player>(`${getUrl()}/${id}`).pipe(
      delayIfDebug(500)
    );
  }

  update(player: Player): Observable<Player> {
    return this.http.put<Player>(getUrl(), player).pipe(
      delayIfDebug(500)
    );
  }

  create(player: Player): Observable<Player> {
    player.id = 0;
    return this.http.post<Player>(getUrl(), player).pipe(
      delayIfDebug(500)
    );
  }

  delete(player: Player): Observable<void> {
    return this.http.delete<void>(`${getUrl()}/${player.id}`).pipe(
      delayIfDebug(500)
    );
  }
}
