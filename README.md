# M&F Web Stack
The repository contains a sample application based on the M&F Web Stack.

## Server

### Prerequisites

The project will require .NET 4.7.1. DO NOT install the framwork from the internet. It has to be installed through the Visual Studio Installer.

To build and run the server, Microsoft Visual Studio 2017 is required at Version >= 15.6. During installation, the `.NET desktop development` workload and the `.NET Framework 4.7.1 development tools` have to be selected.

To run the server, the following command has be run once on an **elevated** command prompt:

```
netsh http add urlacl url=http://+:9000/ user=[Your Username]
```

Beware: this registration can only be made for one windows-user at a time: to unregister an existing entry, type
```
netsh http delete urlacl url=http://+:9000/
```

### Building

Open the Visual Studio solution `source/MnF.WebStack/MnF.WebStack.sln` and build it. The build should complete without errors or warnings.

### Running

Select `TableSoccerServer` as startup project and hit start. After a couple of seconds the server will be listening for HTTP requests on port 9000.

## Client

### Prerequisites

To build and run the client, the following tools and packages have  to be installed:

- [Node.js](https://nodejs.org/en/), version >= 8.11
- [npm](https://www.npmjs.com/get-npm), version >= 6.x
- [angular-cli](https://github.com/angular/angular-cli), version >= 6.x

To check if all is installed as required, open a command prompt and type:

```
> ng --version
```

This should result something like:

```
     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 6.0.3
Node: 8.11.2
OS: win32 x64
Angular: 6.0.2

...
```
### Install dependencies

Now open a command prompt in `source/table-soccer` and type:

```
> npm install
```

This will take a while as you're downloading the internet...

### Running
#### Running Local Dev-Server

- Ensure the `TableSoccerServer` is up and running
- Open a command prompt in `source/table-soccer`
- Type `ng serve` and wait until `Compiled successfully.` appears
- Open a browser of your choice and navigate to <http://localhost:4200/>
- Enjoy!

#### Running Local Prod-Server

 -Terminate `TableSoccerServer`
 - Open a command prompt in `source/table-soccer`
 - Type `ng build --prod` and wait until the files have been Compiled
 - Start `TableSoccerServer` again
 - Open a browser of your choice and navigate to <http://localhost:9000/>
 - Enjoy!


#### Running Azure ASP.Net Server

 - Terminate `TableSoccerServer`
 - Open a command prompt in `source/table-soccer`
 - Type `ng build --prod` and wait until the files have been Compiled
 - Copy the files in `source\table-soccer\dist\table-soccer` to `source\MnF.WebStack\TableSoccerAzureAspServer\Web`

##### Locally

 - Start `TableSoccerAzureAspServer`
 - Open a browser of your choice and navigate to <http://localhost:51015/>
 - Enjoy!

##### On Azure

 - Right-clik on `TableSoccerAzureAspServer` and select `Publish...`. Publish to Azure.
 - Open a browser of your choice and navigate to <http://tablesocceraspserver.azurewebsites.net/spontaneousMatches>
 - Be patient: Azure can be kind of slow

#### Running Azure Function CurrentMatch

##### Locally
 - API only (no GUI)
 - Start `CurrentMatch`
 - Open Postman and send a GET Request to <http://localhost:7071/api/GetCurrentMatch/>

##### On Azure
 - API only (no GUI)
 - Right-Click on CurrentMatch and select `Publish...`. Publish to Azure.
 - Open Postman and send a GET Request to <http://currentmatch.azurewebsites.net/api/GetCurrentMatch>
